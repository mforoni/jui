package com.gitlab.mforoni.jui.layout;

import java.awt.Color;
import java.awt.Component;
import javax.annotation.Nonnull;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class BoxLayoutPanel {
  private final JPanel jpanel;

  public enum Axis {
    X(BoxLayout.X_AXIS), Y(BoxLayout.Y_AXIS);

    private final int value;

    private Axis(final int value) {
      this.value = value;
    }

    int getValue() {
      return value;
    }
  }

  public BoxLayoutPanel(final Axis axis) {
    jpanel = new JPanel();
    jpanel.setLayout(new BoxLayout(jpanel, axis.getValue()));
  }

  public BoxLayoutPanel setBackground(final Color color) {
    jpanel.setBackground(color);
    return this;
  }

  public BoxLayoutPanel border(final Border border) {
    jpanel.setBorder(border);
    return this;
  }

  public BoxLayoutPanel titledBorder(final String title) {
    jpanel.setBorder(new TitledBorder(title));
    return this;
  }

  public BoxLayoutPanel add(@Nonnull final Component component) {
    jpanel.add(component);
    return this;
  }

  public BoxLayoutPanel add(@Nonnull final Component first, @Nonnull final Component second,
      final Component... others) {
    jpanel.add(first);
    jpanel.add(second);
    for (final Component component : others) {
      jpanel.add(component);
    }
    return this;
  }

  @Nonnull
  public JPanel build() {
    return jpanel;
  }
}
