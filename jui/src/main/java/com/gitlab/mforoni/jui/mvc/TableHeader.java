package com.gitlab.mforoni.jui.mvc;

import com.google.common.annotations.Beta;

@Beta
public interface TableHeader<T> {
  public int getColumnCount();

  public String getColumnName(final int columnIndex);

  public Class<?> getColumnClass(final int columnIndex);

  default boolean isEditable() {
    return false;
  }

  default boolean isEditable(final int columnIndex) {
    return false;
  }

  public Object getValue(T instance, int columnIndex);

  default void setValue(final T instance, final int columnIndex, final Object value) {}
}
