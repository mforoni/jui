package com.gitlab.mforoni.jui.mvc.form;

import com.github.mforoni.jbasic.JMaps;
import com.github.mforoni.jbasic.reflect.JFields;
import com.gitlab.mforoni.jui.mvc.form.FormElement.ComponentType;
import com.gitlab.mforoni.jui.mvc.form.FormElement.FormComboBox;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.ComboBoxModel;
import javax.swing.JLabel;

public class FormModel {
  private final Map<String, FormElement<?>> elements = new LinkedHashMap<>();

  public FormModel(final List<FormElement<?>> elements) {
    super();
    for (final FormElement<?> e : elements) {
      JMaps.putEnsuringNoCollision(this.elements, caseInsensentiveKey(e.getLabel().getText()), e);
    }
  }

  private static String caseInsensentiveKey(final String s) {
    return s.toLowerCase();
  }

  public List<FormElement<?>> getElements() {
    return new ArrayList<>(elements.values());
  }

  @Nullable
  public FormElement<?> getElement(final String labelText) {
    return elements.get(caseInsensentiveKey(labelText));
  }

  @Nullable
  public FormComboBox<?> getFormComboBox(final String labelText) {
    final FormElement<?> e = getElement(labelText);
    if (e != null && e.getComponentType() == ComponentType.COMBOBOX) {
      return (FormComboBox<?>) e;
    }
    return null;
  }

  @Nullable
  public <T> FormComboBox<T> getFormComboBox(final String labelText, final Class<T> type) {
    final FormComboBox<?> fc = getFormComboBox(labelText);
    if (fc != null) {
      return (FormComboBox<T>) fc;
    }
    return null;
  }

  @Override
  public String toString() {
    return "FormModel [elements=" + elements + "]";
  }

  public static final class Builder {
    private final List<FormElement<?>> elements = new ArrayList<>();

    public Builder() {}

    public Builder add(final FormElement<?> element) {
      elements.add(element);
      return this;
    }

    public Builder add(final String labelText, final ComponentType componentType) {
      return add(labelText, componentType, null, null);
    }

    public Builder add(final String labelText, final ComponentType componentType,
        @Nullable final ComboBoxModel<?> model, @Nullable final Class<?> type) {
      final FormElement<?> element;
      switch (componentType) {
        case CHECKBOX:
          element = new FormElement.FormCheckBox(new JLabel(labelText));
          break;
        case COMBOBOX:
          Preconditions.checkNotNull(model);
          element = new FormElement.FormComboBox<>(new JLabel(labelText), model);
          break;
        case DATEPICKER:
          element = new FormElement.FormDatePicker(new JLabel(labelText), type);
          break;
        case JLABEL:
          element = new FormElement.FormLabel(new JLabel(labelText));
          break;
        case TEXTFIELD:
          element = new FormElement.FormTextField(new JLabel(labelText), type);
          break;
        case JLIST:
          element = new FormElement.FormList(new JLabel(labelText));
          break;
        default:
          throw new IllegalStateException();
      }
      elements.add(element);
      return this;
    }

    public FormModel build() {
      return new FormModel(elements);
    }
  }

  @Deprecated
  public static FormModel _fromType(@Nonnull final Class<?> type) {
    return fromFields(type, JFields.fromType(type));
  }

  @Deprecated
  public static FormModel _fromType(@Nonnull final Class<?> type,
      final Predicate<Field> predicate) {
    return fromFields(type, JFields.fromType(type, predicate));
  }

  @Deprecated
  private static FormModel fromFields(@Nonnull final Class<?> type, final List<Field> fields) {
    Preconditions.checkNotNull(type);
    final FormModel.Builder builder = new FormModel.Builder();
    for (final Field field : fields) {
      try {
        builder.add(FormElement.fromType(type, field.getName(), !JFields.isFinal(field)));
      } catch (final Throwable t) {
        throw new IllegalStateException("Cannot build FormElement from field " + field, t);
      }
    }
    return builder.build();
  }
}
