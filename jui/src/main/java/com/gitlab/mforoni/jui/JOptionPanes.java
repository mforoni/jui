package com.gitlab.mforoni.jui;

import static com.github.mforoni.jbasic.JStrings.NEWLINE;
import com.google.common.annotations.Beta;
import java.awt.Component;
import javax.swing.JOptionPane;

public final class JOptionPanes {
  private static final String INFO = "Info";
  private static final String WARNING = "Warning";
  private static final String ERROR = "Error";

  private JOptionPanes() {
    throw new AssertionError();
  }

  public enum Choice {
    YES(JOptionPane.YES_OPTION), NO(JOptionPane.NO_OPTION);

    private final int code;

    private Choice(final int code) {
      this.code = code;
    }

    public int getCode() {
      return code;
    }

    static Choice parseInt(final int code) {
      if (code == JOptionPane.YES_OPTION) {
        return YES;
      } else if (code == JOptionPane.NO_OPTION) {
        return NO;
      } else {
        throw new IllegalArgumentException();
      }
    }
  }

  public static void showInformationDialog(final Component parent, final String message) {
    JOptionPane.showMessageDialog(parent, message, INFO, JOptionPane.INFORMATION_MESSAGE);
  }

  public static void showWarningDialog(final Component parent, final String message) {
    JOptionPane.showMessageDialog(parent, message, WARNING, JOptionPane.WARNING_MESSAGE);
  }

  public static void showErrorDialog(final Component parent, final String message) {
    JOptionPane.showMessageDialog(parent, message, ERROR, JOptionPane.ERROR_MESSAGE);
  }

  @Beta
  static void showErrorDialog(final Component parent, final String message, final Exception ex) {
    JOptionPane.showMessageDialog(parent, message.concat(NEWLINE).concat(ex.getLocalizedMessage()),
        ERROR, JOptionPane.ERROR_MESSAGE);
  }

  public static Choice showConfirmDialog(final Component parent, final String message) {
    final int result = JOptionPane.showConfirmDialog(parent, message, "Confirm decision",
        JOptionPane.YES_NO_OPTION);
    return Choice.parseInt(result);
  }
}
