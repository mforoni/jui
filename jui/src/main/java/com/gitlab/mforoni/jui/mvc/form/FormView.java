/** */
package com.gitlab.mforoni.jui.mvc.form;

import com.gitlab.mforoni.jui.JOptionPanes;
import com.gitlab.mforoni.jui.layout.FlowLayoutPanel;
import com.gitlab.mforoni.jui.layout.GroupLayoutPanel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/** @author Foroni Marco */
public final class FormView {
  private final JPanel panel = new JPanel();
  private JPanel confirmPanel;
  private final JButton button = new JButton("Confirm");
  private final FormModel model;

  public FormView(final FormModel model) {
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    this.model = model;
  }

  void addButtonActionListener(final ActionListener l) {
    confirmPanel.setVisible(true);
    button.addActionListener(l);;
  }

  public JButton getButton() {
    return button;
  }

  public JPanel getJPanel() {
    return panel;
  }

  void initialize() {
    final List<JLabel> labels = new ArrayList<>();
    final List<JComponent> components = new ArrayList<>();
    for (final FormElement<?> elem : model.getElements()) {
      labels.add(elem.getLabel());
      components.add(elem.getUIComponent());
    }
    final JPanel centralPanel = new GroupLayoutPanel(labels, components).build();
    panel.add(centralPanel);
    confirmPanel = new FlowLayoutPanel().add(button).build();
    confirmPanel.setVisible(false);
    panel.add(confirmPanel);
  }

  public void showErrorDialog(final String msg) {
    JOptionPanes.showErrorDialog(panel, msg);
  }

  public void showWarningDialog(final String msg) {
    JOptionPanes.showWarningDialog(panel, msg);
  }
}
