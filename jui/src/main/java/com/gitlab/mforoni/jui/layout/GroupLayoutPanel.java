package com.gitlab.mforoni.jui.layout;

import java.util.Iterator;
import java.util.List;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public final class GroupLayoutPanel {
  private final JPanel panel;
  private final List<JLabel> labels;
  private final List<JComponent> fields; // JTextField, JComboBox, ...

  public GroupLayoutPanel(final List<JLabel> labels, final List<JComponent> fields) {
    if (labels.size() != fields.size()) {
      throw new IllegalArgumentException();
    }
    this.labels = labels;
    this.fields = fields;
    panel = new JPanel();
  }

  public JPanel build() {
    final GroupLayout layout = new GroupLayout(panel);
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    if (labels.size() > 0) {
      final SequentialGroup horSeqGroup = layout.createSequentialGroup();
      final ParallelGroup parGroup1 = layout.createParallelGroup(Alignment.LEADING);
      for (final JLabel label : labels) {
        parGroup1.addComponent(label);
      }
      final ParallelGroup parGroup2 = layout.createParallelGroup(Alignment.LEADING);
      for (final JComponent textField : fields) {
        parGroup2.addComponent(textField);
      }
      horSeqGroup.addGroup(parGroup1).addGroup(parGroup2);
      layout.setHorizontalGroup(horSeqGroup);
      final SequentialGroup vertSeqGroup = layout.createSequentialGroup();
      final Iterator<JLabel> iterLabels = labels.iterator();
      final Iterator<JComponent> iterFields = fields.iterator();
      while (iterLabels.hasNext() && iterFields.hasNext()) {
        final ParallelGroup parGroup = layout.createParallelGroup(Alignment.LEADING);
        final JLabel label = iterLabels.next();
        final JComponent comp = iterFields.next();
        parGroup.addComponent(label).addComponent(comp);
        vertSeqGroup.addGroup(parGroup);
      }
      layout.setVerticalGroup(vertSeqGroup);
    }
    panel.setLayout(layout);
    return panel;
  }
}
