package com.gitlab.mforoni.jui;

import javax.annotation.Nullable;
import javax.swing.JTextField;

public final class TextField {
  private final JTextField field;

  public TextField() {
    this(new JTextField());
  }

  public TextField(final JTextField field) {
    super();
    this.field = field;
  }

  public JTextField get() {
    return field;
  }

  public String getText() {
    return field.getText();
  }

  public void setText(final String text) {
    field.setText(text);
  }

  public void setText(@Nullable final Integer value) {
    field.setText(value == null ? "" : value.toString());
  }

  public void setText(final int value) {
    field.setText(String.valueOf(value));
  }

  public void clear() {
    field.setText("");
  }
}
