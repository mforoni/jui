package com.gitlab.mforoni.jui.layout;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 * A {@code JPanel} with {@code GridBagLayout}.
 *
 * @author Foroni
 * @see JPanel
 * @see GridBagLayout
 */
public final class GridBagLayoutPanel {
  private static final int INSETS_TOP_SIZE = 10;
  private static final int INSETS_BOTTOM_SIZE = 10;
  private static final int INSETS_LEFT_SIZE = 10;
  private static final int INSETS_RIGHT_SIZE = 10;
  private final JPanel panel;
  private Insets insets;
  private int maxNumCols = 0;
  private final List<JComponent[]> components;
  private final List<DisplayConstraints> displayConstraints;

  public GridBagLayoutPanel() {
    panel = new JPanel();
    insets = new Insets(INSETS_TOP_SIZE, INSETS_LEFT_SIZE, INSETS_BOTTOM_SIZE, INSETS_RIGHT_SIZE);
    panel.setLayout(new GridBagLayout());
    components = new ArrayList<>();
    displayConstraints = new ArrayList<>();
  }

  public GridBagLayoutPanel setInsets(final Insets insets) {
    this.insets = insets;
    return this;
  }

  public GridBagLayoutPanel setBorder(final String title) {
    panel.setBorder(new TitledBorder(title));
    return this;
  }

  public GridBagLayoutPanel addRow(final DisplayConstraints constraints,
      final JComponent... components) {
    if (components == null || components.length == 0) {
      throw new IllegalArgumentException();
    }
    this.components.add(components);
    displayConstraints.add(constraints);
    if (components.length > maxNumCols) {
      maxNumCols = components.length;
    }
    return this;
  }

  private void addAll() {
    final GridBagConstraints gridBagConstraints = new GridBagConstraints();
    for (int i = 0; i < components.size(); i++) {
      final JComponent[] c = components.get(i);
      final DisplayConstraints constraints = displayConstraints.get(i);
      final int startCols = getStartColumn(maxNumCols, c.length);
      for (int j = 0; j < c.length; j++) {
        if (constraints.getComponentWeightX(j) != null) {
          gridBagConstraints.weightx = constraints.getComponentWeightX(j);
        }
        if (constraints.getComponentAnchor(j) != null) {
          gridBagConstraints.anchor = constraints.getComponentAnchor(j);
        }
        if (constraints.getComponentFillType(j) != null) {
          gridBagConstraints.fill = constraints.getComponentFillType(j);
        }
        gridBagConstraints.insets = insets;
        gridBagConstraints.gridx = startCols + j;
        gridBagConstraints.gridy = i;
        panel.add(c[j], gridBagConstraints);
      }
    }
  }

  /**
   * Returns the start column for each row in order to balance the row with less elements than the
   * row with the highest number of elements.
   *
   * @param maxNumCols
   * @param numCols
   * @return
   */
  private int getStartColumn(final int maxNumCols, final int numCols) {
    if (maxNumCols - 2 >= numCols) {
      return getStartColumn(maxNumCols - 2, numCols) + 1;
    } else {
      return 0;
    }
  }

  public interface DisplayConstraints {
    /**
     * Example:
     *
     * <pre>
     * <code>
     * private final static double[] weigths_x = new double[] { 0.25, 0.75 };
     *
     * private double getComponentHorizontalWeight(int row, int col) {
     *   return weigths_x[col];
     * }
     * </code>
     * </pre>
     *
     * @param col
     * @return
     */
    public Double getComponentWeightX(final int col);

    public Integer getComponentAnchor(final int col);

    public Integer getComponentFillType(final int col);
  }

  public static final DisplayConstraints NO_CONSTRAINTS = new DisplayConstraints() {
    @Override
    public Double getComponentWeightX(final int col) {
      return null;
    }

    @Override
    public Integer getComponentFillType(final int col) {
      return null;
    }

    @Override
    public Integer getComponentAnchor(final int col) {
      return null;
    }
  };

  public JPanel build() {
    addAll();
    return panel;
  }
}
