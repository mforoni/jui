package com.gitlab.mforoni.jui;

import com.google.common.annotations.Beta;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import java.util.Map;
import javax.annotation.Nullable;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;
import net.coderazzi.filters.gui.AutoChoices;
import net.coderazzi.filters.gui.TableFilterHeader;

@Beta
public final class Table {
  private final JTable table;
  @Nullable
  private final TableFilterHeader tableFilterHeader;

  private Table(final JTable table, final boolean filterable) {
    this.table = table;
    tableFilterHeader = filterable ? new TableFilterHeader(table, AutoChoices.ENABLED) : null;
  }

  public Table(final boolean filterable) {
    this(JTables.instance(), filterable);
  }

  public JTable get() {
    return table;
  }

  public boolean isFilterable() {
    return tableFilterHeader != null;
  }

  public void notSelectionable() {
    table.setFocusable(false);
    table.setRowSelectionAllowed(false);
  }

  public ListSelectionModel getSelectionModel() {
    return table.getSelectionModel();
  }

  public int getSelectedRow() {
    final int index = table.getSelectedRow();
    return index != -1 ? table.convertRowIndexToModel(index) : index;
  }

  public int getRowCount() {
    return table.getRowCount();
  }

  public void setModel(final TableModel dataModel) {
    table.setModel(dataModel);
  }

  public void addMouseListener(final MouseListener l) {
    table.addMouseListener(l);
  }

  @Beta
  public void setScrollableViewportSizeToPreferred() {
    table.setPreferredScrollableViewportSize(table.getPreferredSize());
  }

  @Beta
  public void setScrollableViewportSize(final int rows) {
    table.setPreferredScrollableViewportSize(
        new Dimension(table.getPreferredSize().width, table.getRowHeight() * rows));
  }

  public void setSelectedRow(final int row) {
    table.setRowSelectionInterval(row, row);
  }

  public void setHorizontalAlignment(final int column, final Alignment alignment) {
    JTables.setHorizontalAlignment(table, column, alignment);
  }

  public void setHorizontalAlignment(final Alignment alignment) {
    JTables.setHorizontalAlignment(table, alignment);
  }

  public void autofitColumnWidth() {
    JTables.autofitColumnWidth(table);
  }

  public void autofitColumnWidth(final int column) {
    JTables.autofitColumnWidth(table, column);
  }

  @Nullable
  public <T> T get(final int rowIndex, final String columnName, final Class<T> type) {
    final int columnIndex = table.getColumn(columnName).getModelIndex();
    return type.cast(getObject(rowIndex, columnIndex));
  }

  @Nullable
  public Object getObject(final int rowIndex, final String columnName) {
    final int columnIndex = table.getColumn(columnName).getModelIndex();
    return getObject(rowIndex, columnIndex);
  }

  @Nullable
  public Object getObject(final int rowIndex, final int columnIndex) {
    return table.getModel().getValueAt(rowIndex, columnIndex);
  }

  @Nullable
  public Integer getInteger(final int rowIndex, final String columnName) {
    return Integer.class.cast(getObject(rowIndex, columnName));
  }

  @Nullable
  public Integer getInteger(final int rowIndex, final int columnIndex) {
    return Integer.class.cast(getObject(rowIndex, columnIndex));
  }

  @Nullable
  public String getString(final int rowIndex, final String columnName) {
    return String.class.cast(getObject(rowIndex, columnName));
  }

  @Nullable
  public String getString(final int rowIndex, final int columnIndex) {
    return String.class.cast(getObject(rowIndex, columnIndex));
  }

  public static class Builder {
    private final Map<String, String> tooltips;
    private boolean filterable;

    /** @param tooltips a map of tooltips, maps a tooltip text by his column name. */
    public Builder(final Map<String, String> tooltips) {
      this.tooltips = tooltips;
      filterable = false;
    }

    public Builder filterable() {
      filterable = true;
      return this;
    }

    public Table build() {
      return new Table(JTables.instance(tooltips), filterable);
    }
  }
}
