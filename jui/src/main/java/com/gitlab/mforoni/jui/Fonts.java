package com.gitlab.mforoni.jui;

import java.awt.Font;

public class Fonts {
  private Fonts() {
    throw new AssertionError();
  } // Preventing instantiation

  public static final String FONTNAME_TAHOMA = "Tahoma";
  public static final String FONTNAME_SERIF = "Serif";
  public static final String FONTNAME_SANSERIF = "SansSerif";
  public static final String FONTNAME_VERDANA = "Verdana";
  public static final String FONTNAME_HELVETICA = "Helvetica";
  public static final Font TAHOMA_12 = new Font(FONTNAME_TAHOMA, Font.PLAIN, 12);
  public static final Font TAHOMA_14 = new Font(FONTNAME_TAHOMA, Font.PLAIN, 14);
  public static final Font TAHOMA_16 = new Font(FONTNAME_TAHOMA, Font.PLAIN, 16);
  public static final Font VERDANA_12 = new Font(FONTNAME_VERDANA, Font.PLAIN, 12);
  public static final Font SERIF_12 = new Font(FONTNAME_SERIF, Font.PLAIN, 12);
  public static final Font SANSERIF_12 = new Font(FONTNAME_SANSERIF, Font.PLAIN, 12);
  public static final Font BOLD_VERDANA_18 = new Font(FONTNAME_VERDANA, Font.BOLD, 18);
  public static final Font BOLD_HELVETICA_14 = new Font(FONTNAME_HELVETICA, Font.BOLD, 14);
}
