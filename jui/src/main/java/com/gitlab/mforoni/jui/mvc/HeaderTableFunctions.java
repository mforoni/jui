package com.gitlab.mforoni.jui.mvc;

import com.google.common.annotations.Beta;
import java.util.function.BiFunction;
import java.util.function.Function;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Beta
public final class HeaderTableFunctions<T> implements TableHeader<T> {
  public interface VoidTriFunction<T, U, V> {
    public void apply(T arg1, U arg2, V arg3);
  }

  private boolean editable = false;
  private final int columnCount;
  @Nonnull
  private final Function<Integer, Class<?>> columnIndexToClass;
  @Nonnull
  private final Function<Integer, String> columnIndexToName;
  @Nonnull
  private final BiFunction<T, Integer, Object> columnIndexToValue;
  @Nullable
  private final Function<Integer, Boolean> columnIndexToEditable;
  @Nullable
  private final VoidTriFunction<T, Integer, Object> setValue;
  @Nullable
  private final Persistence<T> persistence;

  public HeaderTableFunctions(final int columnCount,
      @Nonnull final Function<Integer, Class<?>> columnIndexToClass,
      @Nonnull final Function<Integer, String> columnIndexToName,
      @Nonnull final BiFunction<T, Integer, Object> columnIndexToValue,
      @Nullable final Function<Integer, Boolean> columnIndexToEditable,
      @Nullable final VoidTriFunction<T, Integer, Object> setValue,
      @Nullable final Persistence<T> persistence) {
    super();
    this.columnCount = columnCount;
    this.columnIndexToClass = columnIndexToClass;
    this.columnIndexToName = columnIndexToName;
    this.columnIndexToValue = columnIndexToValue;
    this.columnIndexToEditable = columnIndexToEditable;
    this.setValue = setValue;
    this.persistence = persistence;
    for (int i = 0; i < columnCount; i++) {
      if (columnIndexToEditable.apply(i)) {
        editable = true;
      }
    }
  }

  @Override
  public int getColumnCount() {
    return columnCount;
  }

  @Override
  public String getColumnName(final int columnIndex) {
    return columnIndexToName.apply(columnIndex);
  }

  @Override
  public Class<?> getColumnClass(final int columnIndex) {
    return columnIndexToClass.apply(columnIndex);
  }

  @Override
  public boolean isEditable(final int columnIndex) {
    return columnIndexToEditable.apply(columnIndex);
  }

  @Override
  public boolean isEditable() {
    return editable;
  }

  @Override
  public Object getValue(final T instance, final int columnIndex) {
    return columnIndexToValue.apply(instance, columnIndex);
  }

  @Override
  public void setValue(final T instance, final int columnIndex, final Object value) {
    setValue.apply(instance, columnIndex, value);
  }
}
