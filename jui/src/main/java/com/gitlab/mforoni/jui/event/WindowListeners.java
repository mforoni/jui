package com.gitlab.mforoni.jui.event;

import com.gitlab.mforoni.jui.JOptionPanes;
import com.gitlab.mforoni.jui.JOptionPanes.Choice;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.Closeable;
import java.io.IOException;
import javax.swing.JFrame;

public final class WindowListeners {
  private WindowListeners() {
    throw new AssertionError();
  }

  public static WindowAdapter closeAndDispose(final Closeable closeable, final JFrame frame) {
    return new WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent e) {
        try {
          closeable.close();
        } catch (final IOException ex) {
          ex.printStackTrace();
        }
        frame.dispose();
      }
    };
  }

  public static WindowAdapter closeAndDispose(final AutoCloseable closeable, final JFrame frame) {
    return new WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent e) {
        try {
          closeable.close();
        } catch (final Exception ex) {
          ex.printStackTrace();
        }
        frame.dispose();
      }
    };
  }

  public static WindowAdapter disposeFrame(final JFrame frame) {
    return new WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent e) {
        frame.dispose();
      }
    };
  }

  public static WindowAdapter disposeAndRestore(final JFrame dispose, final JFrame restore) {
    return new WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent e) {
        dispose.dispose();
        restore.setVisible(true);
      }
    };
  }

  public static WindowAdapter confirmClosing(final JFrame frame, final String msg) {
    return new WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent e) {
        final Choice choice = JOptionPanes.showConfirmDialog(frame, msg);
        if (choice == Choice.YES) {
          frame.dispose();
        }
      }
    };
  }
}
