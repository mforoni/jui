package com.gitlab.mforoni.jui;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

public class Borders {
  public static final Border BLACKLINE_BORDER = BorderFactory.createLineBorder(Color.black);
  public static final Border RAISED_ETCHED_BORDER =
      BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
  public static final Border LOWERED_ETCHED_BORDER =
      BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
  public static final Border RAISED_BEVEL_BORDER = BorderFactory.createRaisedBevelBorder();
  public static final Border LOWERED_BEVEL_BORDER = BorderFactory.createLoweredBevelBorder();
  public static final Border EMPTY_BORDER = new EmptyBorder(10, 10, 10, 10);

  private Borders() {
    throw new AssertionError();
  }
}
