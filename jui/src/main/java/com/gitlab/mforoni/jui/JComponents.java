package com.gitlab.mforoni.jui;

import javax.swing.JComponent;

public final class JComponents {
  private JComponents() {
    throw new AssertionError();
  }

  public static void enable(final JComponent... components) {
    for (final JComponent component : components) {
      component.setEnabled(true);
    }
  }

  public static void disable(final JComponent... components) {
    for (final JComponent component : components) {
      component.setEnabled(false);
    }
  }
}
