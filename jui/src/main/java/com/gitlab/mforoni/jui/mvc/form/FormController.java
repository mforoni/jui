package com.gitlab.mforoni.jui.mvc.form;

import java.awt.event.ActionListener;

public class FormController<M extends FormModel> {
  private final FormView view;
  private final M model;

  public FormController(final M model) {
    this(new FormView(model), model);
  }

  public FormController(final FormView view, final M model) {
    this.view = view;
    this.model = model;
    view.initialize();
  }

  public M getModel() {
    return model;
  }

  public FormView getView() {
    return view;
  }

  public void addButtonActionListener(final ActionListener l) {
    view.addButtonActionListener(l);
  }
}
