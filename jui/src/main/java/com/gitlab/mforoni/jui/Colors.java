package com.gitlab.mforoni.jui;

import java.awt.Color;

/** @author Foroni Marco */
public final class Colors {
  private Colors() {} // Preventing instantiation

  public static final Color RED = new Color(204, 102, 102);
  public static final Color LIGHT_GREEN = new Color(102, 204, 102);
  public static final Color BLUE = new Color(102, 102, 204);
  public static final Color BLUE2 = new Color(96, 128, 255);
  public static final Color DARK_BLUE = new Color(0, 32, 48);
  public static final Color CREAM = new Color(204, 204, 102);
  public static final Color VIOLET = new Color(204, 102, 204);
  public static final Color AZURE = new Color(102, 204, 204);
  public static final Color ORANGE = new Color(218, 170, 0);
  public static final Color GOLD = new Color(192, 192, 0);
  public static final Color LIGHT_AZURE =
      new Color(Integer.parseInt("EA", 16), Integer.parseInt("F4", 16), Integer.parseInt("FF", 16));
}
