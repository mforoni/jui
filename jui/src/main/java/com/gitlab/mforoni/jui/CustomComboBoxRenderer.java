package com.gitlab.mforoni.jui;

import java.awt.Component;
import javax.swing.JList;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

public class CustomComboBoxRenderer extends BasicComboBoxRenderer {
  /** */
  private static final long serialVersionUID = -3848035357353716413L;
  private final String optionalDependency;

  public CustomComboBoxRenderer() {
    this(null);
  }

  public CustomComboBoxRenderer(final String optionalDependency) {
    this.optionalDependency = optionalDependency;
  }

  @Override
  public Component getListCellRendererComponent(final JList list, final Object value,
      final int index, final boolean isSelected, final boolean cellHasFocus) {
    super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    if (value != null) {
      final CustomObject item = (CustomObject) value;
      setText(item.getDisplayValue(optionalDependency));
    }
    return this;
  }

  public class CustomObject {
    private final String displayValue;

    public CustomObject(final String displayValue) {
      this.displayValue = displayValue;
    }

    public String getDisplayValue(final String optionalDependency) {
      return displayValue;
    }
  }
}
