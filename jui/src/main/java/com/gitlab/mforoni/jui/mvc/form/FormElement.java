package com.gitlab.mforoni.jui.mvc.form;

import com.github.mforoni.jbasic.reflect.JTypes;
import com.gitlab.mforoni.jui.JTextFields;
import com.google.common.annotations.Beta;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import org.jdesktop.swingx.JXDatePicker;

/**
 * Represents a single form element consisting of a {@link JLabel} and a {@link JComponent}
 *
 * @author Foroni Marco
 * @see JLabel
 * @see JComponent
 */
public abstract class FormElement<C extends JComponent> {
  public enum ComponentType {
    COMBOBOX, TEXTFIELD, DATEPICKER, CHECKBOX, // editable
    JLABEL, JLIST; // not editable

    @Nonnull
    public static ComponentType parse(final JComponent component) {
      if (component instanceof JCheckBox) {
        return CHECKBOX;
      } else if (component instanceof JComboBox<?>) {
        return COMBOBOX;
      } else if (component instanceof JXDatePicker) {
        return DATEPICKER;
      } else if (component instanceof JTextField) {
        return TEXTFIELD;
      } else if (component instanceof JLabel) {
        return JLABEL;
      } else if (component instanceof JList<?>) {
        return JLIST;
      } else {
        throw new IllegalStateException();
      }
    }
  }

  private final JLabel label;
  private final C component;
  private final ComponentType componentType;
  @Nullable
  private final ComboBoxModel<?> comboBoxModel;

  public FormElement(final JLabel label, final C component) {
    this(label, component, null);
  }

  public FormElement(final JLabel label, final C component, final ComboBoxModel<?> comboBoxModel) {
    super();
    this.label = label;
    this.component = component;
    this.comboBoxModel = comboBoxModel;
    componentType = ComponentType.parse(component);
  }

  public JLabel getLabel() {
    return label;
  }

  public C getComponent() {
    return component;
  }

  public JComponent getUIComponent() {
    return getComponentType() == ComponentType.JLIST ? new JScrollPane(getComponent())
        : getComponent();
  }

  public ComboBoxModel<?> getComboBoxModel() {
    return comboBoxModel;
  }

  public ComponentType getComponentType() {
    return componentType;
  }

  @Override
  public String toString() {
    return "FormElement [label=" + label + ", component=" + component + ", componentType="
        + componentType + "]";
  }

  public abstract void setValue(final Object obj);

  public abstract Object getValue();

  public static class FormList extends FormElement<JList<String>> {
    public FormList(final JLabel label) {
      super(label, new JList<String>());
    }

    @Override
    public void setValue(final Object obj) {
      if (obj != null) {
        if (Collection.class.isAssignableFrom(obj.getClass())) {
          final DefaultListModel<String> model = new DefaultListModel<>();
          final Collection<?> collection = (Collection<?>) obj;
          for (final Object o : collection) {
            model.addElement(o.toString());
          }
          getComponent().setModel(model);
        } else {
          throw new IllegalStateException();
        }
      }
    }

    @Override
    public Object getValue() {
      throw new IllegalStateException();
    }
  }
  public static class FormTextField extends FormElement<JTextField> {
    private static final Map<Class<?>, Function<String, Object>> FUNCTIONS_STRING_TO_OBJ =
        new HashMap<>();
    static {
      FUNCTIONS_STRING_TO_OBJ.put(String.class, o -> {
        return o;
      });
      FUNCTIONS_STRING_TO_OBJ.put(Integer.class, o -> {
        return Integer.parseInt(o);
      });
      FUNCTIONS_STRING_TO_OBJ.put(Long.class, o -> {
        return Long.parseLong(o);
      });
    }
    private final Class<?> type;

    public FormTextField(final JLabel label, final Class<?> type) {
      super(label, new JTextField());
      Preconditions.checkArgument(FUNCTIONS_STRING_TO_OBJ.containsKey(type));
      this.type = type;
    }

    @Override
    public void setValue(final Object obj) {
      getComponent().setText(obj == null ? "" : obj.toString());
    }

    @Override
    public Object getValue() {
      final String uiValue = getComponent().getText();
      final Function<String, Object> function = FUNCTIONS_STRING_TO_OBJ.get(type);
      Preconditions.checkState(function != null, "Type %s not handled", type.getSimpleName());
      // try {
      // return function.apply(uiValue);
      // } catch (final Exception e) {
      // JOptionPanes.showWarningDialog(null, "");
      // return
      // }
      return function.apply(uiValue);
    }
  }
  public static class FormLabel extends FormElement<JTextField> {
    public FormLabel(final JLabel label) {
      super(label, JTextFields.notEditable());
    }

    @Override
    public void setValue(final Object obj) {
      getComponent().setText(obj == null ? "" : obj.toString());
    }

    @Override
    public Object getValue() {
      // not editable
      throw new AssertionError();
    }
  }
  public static class FormCheckBox extends FormElement<JCheckBox> {
    public FormCheckBox(final JLabel label) {
      super(label, new JCheckBox());
    }

    @Override
    public void setValue(final Object obj) {
      Preconditions.checkArgument(obj == null || obj instanceof Boolean);
      if (obj != null) {
        final boolean b = (Boolean) obj;
        getComponent().setSelected(b);
      }
    }

    @Override
    public Boolean getValue() {
      return getComponent().isSelected();
    }
  }
  public static class FormComboBox<T> extends FormElement<JComboBox<T>> {
    public FormComboBox(final JLabel label, final ComboBoxModel<T> comboBoxModel) {
      super(label, new JComboBox<>(comboBoxModel), comboBoxModel);
    }

    @Override
    public void setValue(final Object obj) {
      getComponent().setSelectedItem(obj);
    }

    @Override
    public Object getValue() {
      return getComponent().getSelectedItem();
    }
  }
  public static class FormDatePicker extends FormElement<JXDatePicker> {
    private static final Map<Class<?>, Function<Date, Object>> FUNCTIONS_DATE_TO_OBJ =
        new HashMap<>();
    private static final Map<Class<?>, Function<Object, Date>> FUNCTIONS_OBJ_TO_DATE =
        new HashMap<>();
    static {
      FUNCTIONS_DATE_TO_OBJ.put(Date.class, d -> {
        return d;
      });
      FUNCTIONS_DATE_TO_OBJ.put(org.joda.time.LocalDate.class, d -> {
        return new org.joda.time.LocalDate(d);
      });
      FUNCTIONS_DATE_TO_OBJ.put(LocalDate.class, d -> {
        final Instant instant = d.toInstant();
        return instant.atZone(ZoneId.systemDefault()).toLocalDate();
      });
      FUNCTIONS_OBJ_TO_DATE.put(Date.class, d -> {
        return (Date) d;
      });
      FUNCTIONS_OBJ_TO_DATE.put(org.joda.time.LocalDate.class, d -> {
        return ((org.joda.time.LocalDate) d).toDate();
      });
      FUNCTIONS_OBJ_TO_DATE.put(LocalDate.class, d -> {
        final LocalDate localDate = (LocalDate) d;
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
      });
    }
    private final Class<?> type;

    public FormDatePicker(final JLabel label, final Class<?> type) {
      super(label, new JXDatePicker());
      Preconditions.checkArgument(FUNCTIONS_DATE_TO_OBJ.containsKey(type));
      this.type = type;
    }

    @Override
    public void setValue(final Object obj) {
      if (obj != null) {
        final Function<Object, Date> function = FUNCTIONS_OBJ_TO_DATE.get(type);
        Preconditions.checkState(function != null, "Type %s not handled", type.getSimpleName());
        final Date date = function.apply(obj);
        getComponent().setDate(date);
      }
    }

    @Override
    public Object getValue() {
      final Date uiValue = getComponent().getDate();
      final Function<Date, Object> function = FUNCTIONS_DATE_TO_OBJ.get(type);
      return function.apply(uiValue);
    }
  }

  @Beta
  @Nonnull
  public static <E extends Enum<E>> FormElement<?> fromType(final Class<?> type,
      final String labelText, final boolean editable) {
    if (!editable) {
      if (Collection.class.isAssignableFrom(type)) {
        return new FormList(new JLabel(labelText));
      }
      return new FormLabel(new JLabel(labelText));
    } else if (type.isEnum()) {
      final ComboBoxModel<?> model = new DefaultComboBoxModel<>(type.getEnumConstants());
      return new FormComboBox<>(new JLabel(labelText), model);
    } else if (type.equals(Date.class) || type.equals(LocalDate.class)
        || type.equals(org.joda.time.LocalDate.class)) {
      return new FormDatePicker(new JLabel(labelText), type);
    } else if (JTypes.isBoolean(type)) {
      return new FormCheckBox(new JLabel(labelText));
    } else if (type.equals(String.class)) {
      return new FormTextField(new JLabel(labelText), String.class);
    } else if (type.equals(Integer.class) || type.equals(int.class)) {
      return new FormTextField(new JLabel(labelText), Integer.class);
    } else if (Collection.class.isAssignableFrom(type)) {
      return new FormList(new JLabel(labelText));
    } else {
      throw new IllegalStateException(
          String.format("Cannot build automatically a FormElement for the type %s", type));
    }
  }
}
