package com.gitlab.mforoni.jui.mvc;

import com.gitlab.mforoni.jui.event.TableCellListener;
import com.google.common.base.Preconditions;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.annotation.Nonnull;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.event.ListSelectionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for displaying data on a {@link TableView} according to a {@link ListTableModel}.
 *
 * @author Marco Foroni
 * @param <T>
 * @see TableView
 * @see ListTableModel
 */
public final class TableController<T> {
  private static final Logger LOGGER = LoggerFactory.getLogger(TableController.class);
  @Nonnull
  private final TableView view;
  @Nonnull
  private final ListTableModel<T> tableModel;

  public TableController(@Nonnull final TableView view,
      @Nonnull final ListTableModel<T> tableModel) {
    Preconditions.checkNotNull(view);
    Preconditions.checkNotNull(tableModel);
    this.view = view;
    this.tableModel = tableModel;
    initialize();
  }

  private void initialize() {
    view.setModel(tableModel);
    if (tableModel.isEditable()) {
      // new TableCellListener(view.getTable(), cellChangeAction);
    }
  }

  public TableView getView() {
    return view;
  }

  public ListTableModel<T> getTableModel() {
    return tableModel;
  }

  public void setList(final List<T> list) {
    tableModel.setList(list);
    view.adjustColumns();
  }

  public void addDeleteRowAction(@Nonnull final ActionListener l) {
    view.addDeleteRowActionListener(l);
  }

  public void addInsertRowAction(@Nonnull final ActionListener l) {
    view.addInsertRowActionListener(l);
  }

  public void addListSelectionListener(@Nonnull final ListSelectionListener l) {
    view.addListSelectionListener(l);
  }

  public void addDoubleClickAction(@Nonnull final ActionListener l) {
    view.addDoubleClickAction(l);
  }

  private final Action cellChangeAction = new AbstractAction() {
    /** */
    private static final long serialVersionUID = 1L;

    @Override
    public void actionPerformed(final ActionEvent e) {
      final TableCellListener tcl = (TableCellListener) e.getSource();
      LOGGER.info("Detected cell change at [{}, {}]: old value = {}, new value = {}", tcl.getRow(),
          tcl.getColumn(), tcl.getOldValue(), tcl.getNewValue());
    }
  };
}
