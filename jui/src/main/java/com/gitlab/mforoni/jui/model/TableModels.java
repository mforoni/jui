package com.gitlab.mforoni.jui.model;

import java.util.List;
import javax.annotation.Nonnull;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/** @author Foroni Marco */
public final class TableModels {
  private TableModels() {
    throw new AssertionError();
  }

  public static boolean isEditable(final TableModel tableModel) {
    for (int r = 0; r < tableModel.getRowCount(); r++) {
      for (int c = 0; c < tableModel.getColumnCount(); c++) {
        if (tableModel.isCellEditable(r, c)) {
          return true;
        }
      }
    }
    return false;
  }

  public static DefaultTableModel newNotEditable(@Nonnull final List<Object[]> data,
      @Nonnull final String[] columnNames) {
    return newNotEditable(data.toArray(new Object[data.size()][]), columnNames);
  }

  public static DefaultTableModel newNotEditable(@Nonnull final Object[][] data,
      @Nonnull final String[] columnNames) {
    return new DefaultTableModel(data, columnNames) {
      /** */
      private static final long serialVersionUID = -6670156761439853398L;

      @Override
      public boolean isCellEditable(final int row, final int column) {
        return false;
      }
    };
  }
}
