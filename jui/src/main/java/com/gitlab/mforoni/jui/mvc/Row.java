package com.gitlab.mforoni.jui.mvc;

import java.util.Objects;

final class Row<T> {
  enum State {
    UNCHANGED, CHANGED, NEW, DELETED
  }

  private State state;
  private final T instance;
  private final TableHeader<T> tableHeader;

  Row(final T instance, final TableHeader<T> tableHeader) {
    super();
    this.instance = instance;
    this.tableHeader = tableHeader;
    this.state = State.UNCHANGED;
  }

  public Object getValueAt(final int columnIndex) {
    return tableHeader.getValue(instance, columnIndex);
  }

  public void setValueAt(final int columnIndex, final Object value) {
    switch (state) {
      case NEW:
        tableHeader.setValue(instance, columnIndex, value);
        break;
      case CHANGED:
        tableHeader.setValue(instance, columnIndex, value);
        break;
      case UNCHANGED:
        final Object oldValue = getValueAt(columnIndex);
        if (!Objects.equals(oldValue, value)) { // if changed
          state = State.CHANGED;
          tableHeader.setValue(instance, columnIndex, value);
        }
        break;
      case DELETED:
        throw new IllegalStateException();
    }
  }

  public T getInstance() {
    return instance;
  }

  public State getState() {
    return state;
  }

  public void delete() {
    switch (state) {
      case NEW:
        state = State.DELETED;
        break;
      case UNCHANGED:
        state = State.DELETED;
        break;
      case CHANGED:
        state = State.DELETED;
        break;
      case DELETED:
        throw new IllegalStateException();
    }
  }

  @Override
  public String toString() {
    return String.format("Row [instance=%s]", instance);
  }
}
