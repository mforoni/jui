package com.gitlab.mforoni.jui;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.annotation.Nonnull;

/**
 * The Item class is designed to be used by Swing components that use a renderer to display an
 * Object. Swing renderers rely on the toString() implementation to display a description of an
 * Object. Separating the value and description properties gives increased flexibility.
 *
 * <p>
 * The "description" property is used by the renderer. The "value" property is used for processing
 * by the application.
 *
 * <p>
 * For more info see this
 * <a href="https://tips4java.wordpress.com/2013/02/18/combo-box-with-hidden-data/">link</a>.
 *
 * @author Java Tips Weblog
 * @author Marco Foroni
 * @param <V> the object to wrap. It <b>must properly override</b> {@link Object#equals(Object)} and
 *        {@link Object#hashCode()}.
 */
public class Item<V> implements Comparable<Item<V>> {
  private final V value;
  private final String description;

  /**
   * Create an Item object
   *
   * @param value an Object containing data used by the application
   * @param description the text to be displayed by a renderer
   */
  public Item(@Nonnull final V value, @Nonnull final String description) {
    checkNotNull(value);
    checkNotNull(description);
    this.value = value;
    this.description = description;
  }

  /**
   * Get the Object containing application data
   *
   * @return the application data
   */
  public @Nonnull V getValue() {
    return value;
  }

  /**
   * Get the description of the value data
   *
   * @return the description to be displayed by a renderer
   */
  public @Nonnull String getDescription() {
    return description;
  }

  /**
   * Implement the natural order for this class using the {@code description} property.
   *
   * @param item the other {@code Item} object to be used in the comparison
   */
  @Override
  public int compareTo(final Item<V> item) {
    return getDescription().compareTo(item.getDescription());
  }

  /** The Value property will be used to check for equality */
  @Override
  public boolean equals(final Object object) {
    // final Item item = (Item)object;
    // return value.equals(item.getValue());
    if (object == null) {
      return false;
    }
    if (object == this) {
      return true;
    }
    if (!(object instanceof Item<?>)) {
      return false;
    }
    final Item<?> item = (Item<?>) object;
    if (!value.equals(item.getValue())) {
      return false;
    }
    if (!description.equals(item.getDescription())) {
      return false;
    }
    return true;
  }

  /** The Value property will be used to determine the hashCode */
  @Override
  public int hashCode() {
    // return value.hashCode();
    int result = 17;
    result = 31 * result + value.hashCode();
    result = 31 * result + description.hashCode();
    return result;
  }

  /**
   * The {@code description} property will double as the toString representation.
   *
   * @return the description
   */
  @Override
  public String toString() {
    return description;
  }
}
