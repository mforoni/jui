package com.gitlab.mforoni.jui;

import java.util.Collection;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.JComboBox;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

public final class JComboBoxes {
  private JComboBoxes() {
    throw new AssertionError();
  }

  public static <T> JComboBox<T> withAutocomplete() {
    final JComboBox<T> comboBox = new JComboBox<>();
    AutoCompleteDecorator.decorate(comboBox);
    return comboBox;
  }

  public static <T> JComboBox<T> of(@Nonnull final Collection<T> elements) {
    final JComboBox<T> comboBox = new JComboBox<>();
    load(comboBox, elements);
    AutoCompleteDecorator.decorate(comboBox);
    return comboBox;
  }

  public static <T> JComboBox<T> of(@Nonnull final T[] elements) {
    final JComboBox<T> comboBox = new JComboBox<>(elements);
    AutoCompleteDecorator.decorate(comboBox);
    return comboBox;
  }

  public static <T> void load(@Nonnull final JComboBox<T> comboBox,
      @Nonnull final Collection<T> elements) {
    for (final T element : elements) {
      comboBox.addItem(element);
    }
  }

  public static <T> void load(@Nonnull final JComboBox<T> comboBox, @Nonnull final T[] elements) {
    for (final T element : elements) {
      comboBox.addItem(element);
    }
  }

  /**
   * Returns the selected item of the given {@code comboBox}. Returns <tt>null</tt> if nothing is
   * selected.
   *
   * @param comboBox
   * @return
   */
  @Nullable
  public static <T> T getSelectedItem(@Nonnull final JComboBox<T> comboBox) {
    return comboBox.getItemAt(comboBox.getSelectedIndex());
  }

  public static <T> void setSelectedItem(@Nonnull final JComboBox<T> compoBox,
      @Nonnull final T item) {
    compoBox.setSelectedItem(item);
  }
}
