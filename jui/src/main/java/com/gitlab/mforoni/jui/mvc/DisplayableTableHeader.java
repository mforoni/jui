package com.gitlab.mforoni.jui.mvc;

import com.github.mforoni.jbasic.JExceptions;
import com.github.mforoni.jbasic.JStrings;
import com.github.mforoni.jbasic.reflect.JFields;
import com.github.mforoni.jbasic.reflect.JMethods;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

public class DisplayableTableHeader<T> implements TableHeader<T> {
  private final boolean editable;
  /** Mapping TableColumn objects by index */
  private final Map<Integer, TableColumn> columns = new HashMap<>();

  public static <T> DisplayableTableHeader<T> editable(final Class<T> beanClass) {
    return new DisplayableTableHeader<>(beanClass, true);
  }

  public static <T> DisplayableTableHeader<T> notEditable(final Class<T> beanClass) {
    return new DisplayableTableHeader<>(beanClass, false);
  }

  public DisplayableTableHeader(final Class<T> beanClass, final boolean editable)
      throws IllegalStateException {
    this.editable = editable;
    for (final Field field : JFields.fromType(beanClass)) {
      final DisplayAs displayAs = field.getAnnotation(DisplayAs.class);
      if (displayAs != null) {
        final Method getter = JMethods.nonnullGetter(beanClass, field.getName());
        final TableColumn column = editable
            ? new TableColumn(displayAs.index(), displayAs.value(), getter,
                JMethods.optionalSetter(beanClass, field.getName()))
            : new TableColumn(displayAs.index(), displayAs.value(), getter);
        put(column.getIndex(), column, beanClass);
      }
    }
    for (final Method method : JMethods.ofType(beanClass)) {
      final DisplayAs displayAs = method.getAnnotation(DisplayAs.class);
      if (displayAs != null) {
        final TableColumn column = new TableColumn(displayAs.index(), displayAs.value(), method);
        put(column.getIndex(), column, beanClass);
      }
    }
    if (columns.size() == 0) {
      throw new IllegalStateException();
    }
  }

  private void put(final int index, final TableColumn column, final Class<?> beanClass) {
    if (columns.containsKey(index)) {
      throw JExceptions.newIllegalState(
          "Multiple member annotated with the same index (%s) in class %s", index,
          beanClass.getSimpleName());
    }
    columns.put(index, column);
  }

  @Override
  public void setValue(final T instance, final int columnIndex, final Object value) {
    final TableColumn tableColumn = columns.get(columnIndex);
    final Optional<Method> setter = tableColumn.getSetter();
    if (setter.isPresent()) {
      JMethods.invokeSetter(setter.get(), instance, value);
    }
  }

  @Override
  public Object getValue(final T t, final int columnIndex) {
    final TableColumn tableColumn = getTableColumn(columnIndex);
    try {
      return tableColumn.getGetter().invoke(t);
    } catch (final Exception e) {
      throw new IllegalStateException("Cannot retrieve value for column " + tableColumn, e);
    }
  }

  @Nonnull
  private TableColumn getTableColumn(final int index) throws IllegalArgumentException {
    final TableColumn tableColumn = columns.get(index);
    if (tableColumn == null) {
      throw new IllegalArgumentException("No column defined for index " + index);
    }
    return tableColumn;
  }

  @Override
  public int getColumnCount() {
    return columns.size();
  }

  @Override
  public String getColumnName(final int columnIndex) {
    return getTableColumn(columnIndex).getDisplayName();
  }

  @Override
  public Class<?> getColumnClass(final int columnIndex) {
    return getTableColumn(columnIndex).getGetter().getReturnType();
  }

  @Override
  public boolean isEditable() {
    return editable;
  }

  @Override
  public boolean isEditable(final int columnIndex) {
    return getTableColumn(columnIndex).getSetter().isPresent();
  }

  @Override
  public String toString() {
    final Iterable<String> columns =
        Collections2.transform(this.columns.values(), TableColumn.TO_DESCRIPTION);
    return "DisplayableTableModel: " + JStrings.NEWLINE + Joiner.on(JStrings.NEWLINE).join(columns);
  }

  @Immutable
  private static class TableColumn {
    static final Function<TableColumn, String> TO_DESCRIPTION =
        new Function<DisplayableTableHeader.TableColumn, String>() {
          @Override
          public String apply(final TableColumn tableColumn) {
            return tableColumn != null ? tableColumn.description() : null;
          }
        };
    private final int index;
    private final String displayName;
    private final Method getter;
    @Nullable
    private final Optional<Method> setter;

    TableColumn(final int index, @Nonnull final String label, @Nonnull final Method getter) {
      this(index, label, getter, Optional.<Method>absent());
    }

    TableColumn(final int index, @Nonnull final String label, @Nonnull final Method getter,
        @Nullable final Optional<Method> setter) {
      super();
      Preconditions.checkNotNull(label);
      Preconditions.checkNotNull(getter);
      this.index = index;
      this.displayName = label;
      this.getter = getter;
      this.setter = setter;
    }

    public int getIndex() {
      return index;
    }

    public String getDisplayName() {
      return displayName;
    }

    public Method getGetter() {
      return getter;
    }

    public Optional<Method> getSetter() {
      return setter;
    }

    public String description() {
      return toString() + " " + getter + " " + setter;
    }

    @Override
    public String toString() {
      return displayName + " at " + index;
    }
  }
}
