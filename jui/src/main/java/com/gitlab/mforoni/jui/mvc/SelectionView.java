package com.gitlab.mforoni.jui.mvc;

import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.JComboBoxes;
import com.gitlab.mforoni.jui.OnClose;
import com.gitlab.mforoni.jui.layout.BoxLayoutPanel;
import com.gitlab.mforoni.jui.layout.BoxLayoutPanel.Axis;
import com.gitlab.mforoni.jui.layout.FlowLayoutPanel;
import java.awt.event.ActionListener;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public final class SelectionView<E> {
  private final JLabel label;
  private final JComboBox<E> comboBox = new JComboBox<>();
  private final JButton confirm = new JButton("Ok");
  private final JPanel panel;

  public SelectionView(final String text) {
    label = new JLabel(text);
    final JPanel selection = new FlowLayoutPanel().add(label, comboBox).build();
    final JPanel button = new FlowLayoutPanel(confirm).build();
    panel = new BoxLayoutPanel(Axis.Y).add(selection, button).build();
  }

  void initialize(final ComboBoxModel<E> model, final ActionListener action) {
    comboBox.setModel(model);
    confirm.addActionListener(action);
  }

  public E getSelectedItem() {
    return JComboBoxes.getSelectedItem(comboBox);
  }

  public JPanel getPanel() {
    return panel;
  }

  public Frame newFrame(final String title, final OnClose onClose) {
    final Frame frame = new Frame.Builder(title, onClose).centerPanel(panel).build();
    return frame;
  }
}
