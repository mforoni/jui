package com.gitlab.mforoni.jui;

import com.google.common.annotations.Beta;
import javax.annotation.Nonnull;
import javax.swing.JTextField;

@Beta
public final class JTextFields {
  private JTextFields() {
    throw new AssertionError();
  }

  public static JTextField notEditable() {
    final JTextField textField = new JTextField();
    textField.setEditable(false);
    return textField;
  }

  public static void clear(@Nonnull final JTextField first, final JTextField... others) {
    first.setText("");
    for (final JTextField field : others) {
      field.setText("");
    }
  }
}
