package com.gitlab.mforoni.jui;

import javax.swing.SwingConstants;

public enum Alignment {
  CENTER(SwingConstants.CENTER), LEFT(SwingConstants.LEFT), RIGHT(SwingConstants.RIGHT);

  private final int code;

  private Alignment(final int code) {
    this.code = code;
  }

  /**
   * Gets the swing constant value.
   *
   * @return the swing constant value
   * @see SwingConstants#CENTER
   * @see SwingConstants#LEFT
   * @see SwingConstants#RIGHT
   */
  public int getSwingConstantValue() {
    return code;
  }
}
