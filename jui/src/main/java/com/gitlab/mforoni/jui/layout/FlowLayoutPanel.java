package com.gitlab.mforoni.jui.layout;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import javax.annotation.Nonnull;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 * A {@code JPanel} with {@code FlowLayout}.
 *
 * @author Marco Foroni
 * @see JPanel
 * @see FlowLayout
 */
public final class FlowLayoutPanel {
  public static final int DEFAULT_VGAP = 10;
  public static final int DEFAULT_HGAP = 10;
  private final JPanel jpanel;
  private final FlowLayout flowLayout;

  public FlowLayoutPanel(@Nonnull final Component component) {
    this();
    jpanel.add(component);
  }

  public FlowLayoutPanel() {
    flowLayout = new FlowLayout();
    flowLayout.setVgap(DEFAULT_VGAP);
    flowLayout.setHgap(DEFAULT_HGAP);
    jpanel = new JPanel();
  }

  public FlowLayoutPanel setBackground(final Color color) {
    jpanel.setBackground(color);
    return this;
  }

  public FlowLayoutPanel align(final int align) {
    flowLayout.setAlignment(align);
    return this;
  }

  public FlowLayoutPanel hGap(final int hgap) {
    flowLayout.setHgap(hgap);
    return this;
  }

  public FlowLayoutPanel vGap(final int vgap) {
    flowLayout.setVgap(vgap);
    return this;
  }

  public FlowLayoutPanel border(final Border border) {
    jpanel.setBorder(border);
    return this;
  }

  public FlowLayoutPanel titledBorder(final String title) {
    jpanel.setBorder(new TitledBorder(title));
    return this;
  }

  public FlowLayoutPanel add(@Nonnull final Component component) {
    jpanel.add(component);
    return this;
  }

  public FlowLayoutPanel add(@Nonnull final Component component, final Component... components) {
    jpanel.add(component);
    for (final Component c : components) {
      jpanel.add(c);
    }
    return this;
  }

  @Nonnull
  public JPanel build() {
    jpanel.setLayout(flowLayout);
    return jpanel;
  }
}
