package com.gitlab.mforoni.jui.event;

import com.google.common.annotations.Beta;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Beta
public final class EventListeners {
  private static final Logger LOGGER = LoggerFactory.getLogger(EventListeners.class);

  private EventListeners() {
    throw new AssertionError();
  }

  public static final ListSelectionListener LOG_SELECTION_LISTENER = new ListSelectionListener() {
    @Override
    public void valueChanged(final ListSelectionEvent e) {
      LOGGER.info("Detected selection ListSelectionEvent {}", e);
    }
  };

  public static final ListSelectionListener logSelectedRow(final JTable table) {
    return new ListSelectionListener() {
      @Override
      public void valueChanged(final ListSelectionEvent e) {
        LOGGER.info("Detected selection row {}", table.getSelectedRow());
      }
    };
  }
}
