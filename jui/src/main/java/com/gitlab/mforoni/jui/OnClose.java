package com.gitlab.mforoni.jui;

import javax.swing.WindowConstants;

public enum OnClose {
  DO_NOTHING(0), HIDE(1), DISPOSE(2), EXIT(3);

  private final int code;

  OnClose(final int code) {
    this.code = code;
  }

  /**
   * Returns the corresponding {@link WindowConstants}.
   *
   * @return
   * @see WindowConstants
   */
  public int getCode() {
    return code;
  }
}
