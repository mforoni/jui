package com.gitlab.mforoni.jui.mvc;

import com.github.mforoni.jbasic.reflect.JTypes;
import com.gitlab.mforoni.jui.JTables;
import com.gitlab.mforoni.jui.TableColumnAdjuster;
import com.gitlab.mforoni.jui.layout.FlowLayoutPanel;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import net.coderazzi.filters.Filter;
import net.coderazzi.filters.gui.AutoChoices;
import net.coderazzi.filters.gui.TableFilterHeader;
import org.jdesktop.swingx.table.DatePickerCellEditor;

public final class TableView {
  private static final int DEFAULT_TABLE_SPACING = 15;
  private final JPanel panel = new JPanel();
  private final JButton addButton = new JButton("Add");
  private final JButton deleteButton = new JButton("Delete");
  private final JTable table = new JTable();
  private final JScrollPane tableScroll = new JScrollPane(table);
  @Nullable
  private final TableColumnAdjuster tableColumnAdjuster;
  @Nullable
  private final TableFilterHeader tableFilterHeader;
  private final JPanel buttonsPanel = new FlowLayoutPanel().add(addButton, deleteButton).build();
  private final TableOptions tableOptions;

  TableView() {
    this(TableOptions.DEFAULT);
  }

  public TableView(final TableOptions tableOptions) {
    this.tableOptions = tableOptions;
    addButton.setVisible(false);
    deleteButton.setVisible(false);
    tableFilterHeader =
        tableOptions.isFilterable() ? new TableFilterHeader(table, AutoChoices.ENABLED) : null;
    tableColumnAdjuster =
        tableOptions.isAutosize() ? new TableColumnAdjuster(table, DEFAULT_TABLE_SPACING) : null;
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    panel.add(tableScroll);
    panel.add(buttonsPanel);
  }

  /**
   * Initialize this View.
   *
   * @param tableModel
   */
  void setModel(final ListTableModel<?> tableModel) {
    table.setModel(tableModel);
    if (tableModel.isEditable()) {
      buttonsPanel.setVisible(true);
      buttonsPanel.setEnabled(true);
    }
    for (int c = 0; c < tableModel.getColumnCount(); c++) {
      if (tableModel.isEditble(c)) {
        final Class<?> columnType = tableModel.getColumnClass(c);
        if (JTypes.isDateOrLocalDate(columnType)) {
          final String columnName = tableModel.getColumnName(c);
          final TableColumn dateColumn = table.getColumn(columnName);
          dateColumn.setCellEditor(new DatePickerCellEditor());
        } else if (columnType.isEnum()) {
          final ComboBoxModel<?> model = new DefaultComboBoxModel<>(columnType.getEnumConstants());
          final String columnName = tableModel.getColumnName(c);
          final TableColumn tableColumn = table.getColumn(columnName);
          JTables.setComboBoxColumn(tableColumn, model);
        }
      }
    }
    if (!tableOptions.isHorizontalScroll()) {
      // FIXME not working
      tableScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      table.setPreferredScrollableViewportSize(table.getPreferredSize());
    }
    if (tableOptions.isAutosize()) {
      // table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
      table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
      tableColumnAdjuster.adjustColumns();
    }
    JTables.setHorizontalAlignment(table, tableOptions.getAlignment());
  }

  public void addFilter(final Filter filter) {
    if (tableFilterHeader != null) {
      tableFilterHeader.addFilter(filter);
    }
  }

  public JPanel getPanel() {
    return panel;
  }

  public int getSelectedRow() {
    final int index = table.getSelectedRow();
    return index != -1 ? table.convertRowIndexToModel(index) : index;
  }

  public void adjustColumns() {
    if (tableColumnAdjuster != null) {
      tableColumnAdjuster.adjustColumns();
    }
  }

  public void addDeleteRowActionListener(@Nonnull final ActionListener l) {
    deleteButton.setVisible(true);
    deleteButton.addActionListener(l);
  }

  public void addInsertRowActionListener(@Nonnull final ActionListener l) {
    addButton.setVisible(true);
    addButton.addActionListener(l);
  }

  public void addDoubleClickAction(@Nonnull final ActionListener l) {
    table.addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(final MouseEvent mouseEvent) {
        final JTable table = (JTable) mouseEvent.getSource();
        final Point point = mouseEvent.getPoint();
        final int row = table.rowAtPoint(point);
        if (mouseEvent.getClickCount() == 2) {
          l.actionPerformed(
              new ActionEvent(row, ActionEvent.ACTION_PERFORMED, "Double click on row " + row));
        }
      }
    });
  }

  public void addListSelectionListener(@Nonnull final ListSelectionListener l) {
    table.getSelectionModel().addListSelectionListener(l);
  }
}
