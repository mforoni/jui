package com.gitlab.mforoni.jui.mvc;

import java.awt.event.ActionListener;
import javax.swing.ComboBoxModel;

/** @author Foroni Marco */
public final class SelectionController<E> {
  private final ComboBoxModel<E> model;
  private final SelectionView<E> view;

  public SelectionController(final SelectionView<E> view, final ComboBoxModel<E> model,
      final ActionListener actionListener) {
    this.model = model;
    this.view = view;
    view.initialize(model, actionListener);
  }

  public SelectionView<E> getView() {
    return view;
  }

  public E getSelectedItem() {
    return view.getSelectedItem();
  }
}
