package com.gitlab.mforoni.jui;

import static com.gitlab.mforoni.jui.Borders.BLACKLINE_BORDER;
import static com.gitlab.mforoni.jui.Borders.LOWERED_BEVEL_BORDER;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.JLabel;
import javax.swing.border.Border;

public final class JLabels {
  private JLabels() {
    throw new AssertionError();
  }

  public static void setObject(@Nonnull final JLabel label, @Nullable final Object value) {
    if (value != null) {
      label.setText(value.toString());
    }
  }

  public static void setInt(@Nonnull final JLabel label, final int value) {
    label.setText(String.valueOf(value));
  }

  public static JLabel bordered() {
    return newBorderedLabel(null, BLACKLINE_BORDER);
  }

  public static JLabel bordered(@Nullable final String text) {
    return newBorderedLabel(text, BLACKLINE_BORDER);
  }

  public static JLabel loweredBordered() {
    return newBorderedLabel(null, LOWERED_BEVEL_BORDER);
  }

  public static JLabel loweredBordered(@Nullable final String text) {
    return newBorderedLabel(text, LOWERED_BEVEL_BORDER);
  }

  public static JLabel bordered(@Nonnull final Border border) {
    return newBorderedLabel(null, border);
  }

  public static JLabel bordered(@Nullable final String text, @Nonnull final Border border) {
    return newBorderedLabel(text, border);
  }

  private static JLabel newBorderedLabel(@Nullable final String text,
      @Nonnull final Border border) {
    final JLabel label = text != null ? new JLabel(text) : new JLabel();
    label.setBorder(border);
    return label;
  }

  public static void clear(@Nonnull final JLabel first, final JLabel... others) {
    first.setText("");
    for (final JLabel label : others) {
      label.setText("");
    }
  }
}
