package com.gitlab.mforoni.jui;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class MultiSelect<T> {
  private final JPopupMenu menu = new JPopupMenu();
  private final List<JCheckBoxMenuItem> menuItems = new ArrayList<>();
  private final List<T> items = new ArrayList<>();
  private final JButton button;

  public MultiSelect() {
    this("Click me");
  }

  public MultiSelect(final String text) {
    button = new JButton(text);
    button.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        if (!menu.isVisible()) {
          final Point p = button.getLocationOnScreen();
          menu.setInvoker(button);
          menu.setLocation((int) p.getX(), (int) p.getY() + button.getHeight());
          menu.setVisible(true);
        } else {
          menu.setVisible(false);
        }
      }
    });
  }

  public void removeAllItems() {
    setItems(new ArrayList<>());
  }

  public void setItems(final List<T> items) {
    for (final JCheckBoxMenuItem menuItem : menuItems) {
      menu.remove(menuItem);
    }
    this.items.clear();
    menuItems.clear();
    for (final T item : items) {
      this.items.add(item);
      final JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem(item.toString());
      menuItems.add(menuItem);
      menu.add(menuItem);
    }
    for (final JMenuItem menuItem : menuItems) {
      menuItem.addActionListener(new OpenAction(menu, button));
    }
  }

  public JComponent get() {
    return button;
  }

  public List<T> getSelectedItems() {
    final List<T> list = new ArrayList<>();
    for (int i = 0; i < items.size(); i++) {
      final JCheckBoxMenuItem menuItem = menuItems.get(i);
      if (menuItem.isSelected()) {
        list.add(items.get(i));
      }
    }
    return list;
  }

  private static class OpenAction implements ActionListener {
    private final JPopupMenu menu;
    private final JButton button;

    private OpenAction(final JPopupMenu menu, final JButton button) {
      this.menu = menu;
      this.button = button;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
      menu.show(button, 0, button.getHeight());
    }
  }
}
