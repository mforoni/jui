package com.gitlab.mforoni.jui;

import com.google.common.annotations.Beta;
import java.awt.Point;

/** @author Foroni Marco */
@Beta
public final class Points {
  // Suppresses default constructor, ensuring non-instantiability.
  private Points() {
    throw new AssertionError();
  }

  public static int getMinY(final Point[] points) {
    int min = points[0].y;
    for (int i = 1; i < points.length; i++) {
      min = Math.min(min, points[i].y);
    }
    return min;
  }

  public static int getMinX(final Point[] points) {
    int min = points[0].x;
    for (int i = 1; i < points.length; i++) {
      min = Math.min(min, points[i].x);
    }
    return min;
  }
}
