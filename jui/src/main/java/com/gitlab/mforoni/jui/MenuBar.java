package com.gitlab.mforoni.jui;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/** @author Foroni Marco */
public final class MenuBar {
  private final JMenuBar jmenuBar;
  private final Map<String, Menu> menus;
  private final Set<Character> mnemonics;

  /**
   * Instantiates a new menu bar.
   *
   * @param menu the menu
   * @throws IllegalArgumentException
   */
  public MenuBar(final Menu menu) {
    jmenuBar = new JMenuBar();
    menus = new HashMap<>();
    mnemonics = new HashSet<>();
    add(menu);
  }

  public MenuBar add(final Menu menu) {
    final JMenu jmenu = menu.toJMenu();
    menus.put(jmenu.getText(), menu);
    setMnemonic(jmenu);
    jmenuBar.add(jmenu);
    for (final JMenuItem item : menu.items.values()) {
      setMnemonic(item);
    }
    return this;
  }

  public JMenuItem getJMenuItem(final String path) {
    final String[] split = path.split("->");
    if (split.length != 2) {
      throw new IllegalStateException();
    }
    final Menu menu = menus.get(split[0]);
    if (menu == null) {
      throw new IllegalArgumentException("No Menu found with key " + split[0]);
    }
    final JMenuItem menuItem = menu.items.get(split[1]);
    if (menuItem == null) {
      throw new IllegalArgumentException("No JMenuItem found with key " + split[1]);
    }
    return menuItem;
  }

  public JMenuBar toJMenuBar() {
    return jmenuBar;
  }

  private <M extends JMenuItem> void setMnemonic(final M menuItem) {
    final String name = menuItem.getText();
    final Integer keyCode = getFirstNotUsedKeyCode(name);
    if (keyCode != -1) {
      menuItem.setMnemonic(keyCode);
    }
  }

  private int getFirstNotUsedKeyCode(final String name) {
    for (int i = 0; i < name.length(); i++) {
      final char first = name.charAt(i);
      if (!mnemonics.contains(first)) {
        mnemonics.add(first);
        return KeyEvent.getExtendedKeyCodeForChar(first);
      }
    }
    return -1;
  }

  /**
   * The companion builder class for a {@code JMenu}.
   *
   * @author Foroni
   * @see JMenu
   */
  public static final class Menu {
    private final JMenu jmenu;
    private final Map<String, JMenuItem> items;

    public Menu(final String name) {
      jmenu = new JMenu(name);
      items = new HashMap<>();
    }

    public Menu tooltip(final String text) {
      jmenu.setToolTipText(text);
      return this;
    }

    public Menu item(final String name) {
      return item(name, null, null);
    }

    public Menu item(final String name, final String tooltipText) {
      return item(name, tooltipText, null);
    }

    public Menu item(final String name, final ActionListener al) {
      return item(name, null, al);
    }

    public Menu item(@Nonnull final String name, @Nullable final String tooltipText,
        @Nullable final ActionListener al) {
      final JMenuItem jmenuItem = new JMenuItem(name);
      final JMenuItem item = items.put(name, jmenuItem);
      if (item != null) {
        throw new IllegalStateException("JMenuItem with name " + name + " already added!");
      }
      if (tooltipText != null) {
        jmenuItem.setToolTipText(tooltipText);
      }
      if (al != null) {
        jmenuItem.addActionListener(al);
      }
      jmenu.add(jmenuItem);
      return this;
    }

    public JMenu toJMenu() {
      return jmenu;
    }
  }
}
