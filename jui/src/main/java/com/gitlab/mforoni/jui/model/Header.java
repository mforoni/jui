package com.gitlab.mforoni.jui.model;

import com.gitlab.mforoni.jui.Alignment;
import com.gitlab.mforoni.jui.mvc.TableHeader;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public final class Header<T> implements TableHeader<T> {
  private final boolean editable;
  private final ImmutableList<TableColumn<T>> columns;

  private Header(final boolean editable, final ImmutableList<TableColumn<T>> columns) {
    super();
    this.editable = editable;
    this.columns = columns;
  }

  @Override
  public boolean isEditable() {
    return editable;
  }

  public ImmutableList<TableColumn<T>> getColumns() {
    return columns;
  }

  public int getSize() {
    return columns.size();
  }

  public String getLabel(final int index) {
    return columns.get(index).getLabel();
  }

  public Class<?> getType(final int index) {
    return columns.get(index).getType();
  }

  public Alignment getAlignment(final int index) {
    return columns.get(index).getAlignment();
  }

  public Gettable<T> getGettable(final int index) {
    return columns.get(index).getGettable();
  }

  public Settable<T> getSettable(final int index) {
    return columns.get(index).getSettable();
  }

  @Override
  public String toString() {
    return columns.toString();
  }

  public static class Builder<T> {
    private final List<TableColumn<T>> columns = new ArrayList<>();
    private boolean editable = false;

    public Builder() {}

    public Builder(@Nonnull final String label, @Nonnull final Class<?> type,
        @Nonnull final Gettable<T> gettable) {
      columns.add(new TableColumn<>(label, type, Alignment.CENTER, gettable));
    }

    public Builder(@Nonnull final String label, @Nonnull final Class<?> type,
        @Nonnull final Gettable<T> gettable, final Settable<T> settable) {
      editable = settable != null;
      columns.add(new TableColumn<>(label, type, Alignment.CENTER, gettable, settable));
    }

    public Builder<T> add(@Nonnull final String label, @Nonnull final Class<?> type,
        @Nonnull final Gettable<T> gettable) {
      columns.add(new TableColumn<>(label, type, Alignment.CENTER, gettable));
      return this;
    }

    public Builder<T> add(@Nonnull final String label, @Nonnull final Class<?> type,
        @Nonnull final Gettable<T> gettable, final Settable<T> settable) {
      editable |= settable != null;
      columns.add(new TableColumn<>(label, type, Alignment.CENTER, gettable, settable));
      return this;
    }

    public Header<T> build() {
      return new Header<>(editable, ImmutableList.copyOf(columns));
    }

    @Override
    public String toString() {
      return columns.toString();
    }
  }
  static class TableColumn<T> {
    @Nonnull
    private final String label;
    @Nonnull
    private final Class<?> type;
    @Nonnull
    private final Alignment alignment;
    @Nonnull
    private final Gettable<T> gettable;
    @Nullable
    private final Settable<T> settable;

    TableColumn(@Nonnull final String label, @Nonnull final Class<?> type,
        @Nonnull final Alignment alignment, @Nonnull final Gettable<T> gettable) {
      this(label, type, alignment, gettable, null);
    }

    TableColumn(@Nonnull final String label, @Nonnull final Class<?> type,
        @Nonnull final Alignment alignment, @Nonnull final Gettable<T> gettable,
        @Nullable final Settable<T> settable) {
      super();
      this.label = label;
      this.type = type;
      this.alignment = alignment;
      this.gettable = gettable;
      this.settable = settable;
    }

    @Nonnull
    public String getLabel() {
      return label;
    }

    @Nonnull
    public Class<?> getType() {
      return type;
    }

    @Nonnull
    public Alignment getAlignment() {
      return alignment;
    }

    @Nonnull
    public Gettable<T> getGettable() {
      return gettable;
    }

    @Nullable
    public Settable<T> getSettable() {
      return settable;
    }

    @Override
    public String toString() {
      return label + " (" + type + ")";
    }
  }
  public interface Gettable<T> {
    Object get(T instance);
  }
  public interface Settable<T> {
    public void set(final T instance, final Object value);
  }

  @Override
  public int getColumnCount() {
    return getSize();
  }

  @Override
  public String getColumnName(final int columnIndex) {
    return getLabel(columnIndex);
  }

  @Override
  public Class<?> getColumnClass(final int columnIndex) {
    return getType(columnIndex);
  }

  @Override
  public boolean isEditable(final int columnIndex) {
    return getSettable(columnIndex) != null;
  }

  @Override
  public Object getValue(final T instance, final int columnIndex) {
    return getGettable(columnIndex).get(instance);
  }

  @Override
  public void setValue(final T instance, final int columnIndex, final Object value) {
    final Settable<T> settable = getSettable(columnIndex);
    settable.set(instance, value);
  }
}
