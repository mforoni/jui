package com.gitlab.mforoni.jui.layout;

import com.gitlab.mforoni.jui.Borders;
import java.awt.Component;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 * A {@code JPanel} with {@code GridLayout}.
 *
 * @author Foroni Marco
 * @see JPanel
 * @see GridLayout
 */
public final class GridLayoutPanel {
  public static final int DEFAULT_VGAP = 20;
  public static final int DEFAULT_HGAP = 20;
  private final JPanel jpanel;
  private final GridLayout gridLayout;

  public GridLayoutPanel(final int rows, final int cols) {
    gridLayout = new GridLayout(rows, cols, DEFAULT_HGAP, DEFAULT_VGAP);
    jpanel = new JPanel();
  }

  public GridLayoutPanel hGap(final int hgap) {
    gridLayout.setHgap(hgap);
    return this;
  }

  public GridLayoutPanel vGap(final int vgap) {
    gridLayout.setVgap(vgap);
    return this;
  }

  public GridLayoutPanel border(final Border border) {
    jpanel.setBorder(border);
    return this;
  }

  public GridLayoutPanel emptyBorder() {
    jpanel.setBorder(Borders.EMPTY_BORDER);
    return this;
  }

  public GridLayoutPanel titledBorder(final String title) {
    jpanel.setBorder(new TitledBorder(title));
    return this;
  }

  public GridLayoutPanel add(final Component component) {
    jpanel.add(component);
    return this;
  }

  public JPanel build() {
    // if (jpanel.getComponents().length != gridLayout.getRows() * gridLayout.getColumns()) {
    // throw new IllegalStateException(
    // "The number of JPanel's components is not congruent with GridLayout dimensions");
    // }
    jpanel.setLayout(gridLayout);
    return jpanel;
  }
}
