package com.gitlab.mforoni.jui.mvc;

import com.gitlab.mforoni.jui.TableColumnAdjuster;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class FilterableTableController implements ActionListener {
  private final FilterableTableView view;
  private final TableModel tableModel;
  private final TableRowSorter<TableModel> sorter;

  public FilterableTableController(final FilterableTableView view, final TableModel tableModel) {
    this.view = view;
    this.tableModel = tableModel;
    sorter = new TableRowSorter<>(tableModel);
    initialize();
  }

  private void initialize() {
    view.getFilterButton().addActionListener(this);
    view.getTable().setRowSorter(sorter);
    final JTable table = view.getTable();
    table.setModel(tableModel);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    final TableColumnAdjuster tableColumnAdjuster = new TableColumnAdjuster(table);
    tableColumnAdjuster.adjustColumns();
  }

  @Override
  public void actionPerformed(final ActionEvent e) {
    final String text = view.getTextField().getText();
    // filter on all columns
    final RowFilter<TableModel, Object> rf = RowFilter.regexFilter(text);
    sorter.setRowFilter(rf);
  }

  public FilterableTableView getView() {
    return view;
  }

  public TableModel getTableModel() {
    return tableModel;
  }
}
