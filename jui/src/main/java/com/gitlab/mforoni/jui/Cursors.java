package com.gitlab.mforoni.jui;

import java.awt.Cursor;
import javax.swing.JComponent;
import javax.swing.RootPaneContainer;

/** @author Foroni Marco */
public final class Cursors {
  public static final Cursor WAIT_CURSOR = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
  public static final Cursor DEFAULT_CURSOR = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);

  /**
   * Sets cursor for specified <code>component</code> to <code>WAIT_CURSOR</code>.
   *
   * @param component
   */
  public static void startWaitCursor(final JComponent component) {
    final RootPaneContainer rootPaneContainer = (RootPaneContainer) component.getTopLevelAncestor();
    rootPaneContainer.getGlassPane().setCursor(WAIT_CURSOR);
    rootPaneContainer.getGlassPane().setVisible(true);
  }

  /**
   * Sets cursor for specified <code>component</code> to <code>DEFAULT_CURSOR</code>.
   *
   * @param component
   */
  public static void stopWaitCursor(final JComponent component) {
    final RootPaneContainer rootPaneContainer = (RootPaneContainer) component.getTopLevelAncestor();
    rootPaneContainer.getGlassPane().setCursor(DEFAULT_CURSOR);
    rootPaneContainer.getGlassPane().setVisible(false);
  }
}
