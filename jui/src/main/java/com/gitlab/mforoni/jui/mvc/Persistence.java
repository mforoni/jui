package com.gitlab.mforoni.jui.mvc;

import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Persistence<T> {
  private static final Logger LOGGER = LoggerFactory.getLogger(Persistence.class);

  public void insert(final T instance) {
    LOGGER.info("Inserting object: {}", instance);
  }

  public void update(final T instance) {
    LOGGER.info("Updating object: {}", instance);
  }

  public void delete(final T instance) {
    LOGGER.info("Deleting object: {}", instance);
  }

  public void save(final Collection<Row<T>> rows) {
    for (final Row<T> row : rows) {
      switch (row.getState()) {
        case NEW:
          insert(row.getInstance());
          break;
        case CHANGED:
          update(row.getInstance());
          break;
        case DELETED:
          delete(row.getInstance());
        case UNCHANGED:
          // do nothing
          break;
        default:
          throw new AssertionError();
      }
    }
  }
}
