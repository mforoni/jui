package com.gitlab.mforoni.jui.layout;

import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 * {@code JPanel} with {@code SpringLayout}.
 *
 * @author Foroni
 * @see JPanel
 * @see SpringLayout
 */
public final class SpringLayoutPanel {
  public static final int DEFAULT_INITIAL_X = 5;
  public static final int DEFAULT_INITIAL_Y = 5;
  public static final int DEFAULT_XPAD = 10;
  public static final int DEFAULT_YPAD = 10;
  public Border border = BorderFactory.createEmptyBorder(30, 50, 30, 50); // TODO DA TESTARE
  private final int rows;
  private final int cols;
  final JPanel panel;
  final SpringLayout springLayout;

  public SpringLayoutPanel(final int rows, final int cols) {
    this.rows = rows;
    this.cols = cols;
    springLayout = new SpringLayout();
    panel = new JPanel();
  }

  public SpringLayoutPanel border(final Border border) {
    panel.setBorder(border);
    return this;
  }

  public SpringLayoutPanel titledBorder(final String title) {
    panel.setBorder(new TitledBorder(title));
    return this;
  }

  public SpringLayoutPanel add(final Component component) {
    panel.add(component);
    return this;
  }

  public JPanel build() {
    if (panel.getComponents().length != rows * cols) {
      throw new IllegalStateException(
          "The number of JPanel's components is not congruent with SpringLayout dimensions");
    }
    panel.setLayout(springLayout);
    SpringUtilities.makeCompactGrid(panel, rows, cols, DEFAULT_INITIAL_X, DEFAULT_INITIAL_Y,
        DEFAULT_XPAD, DEFAULT_YPAD);
    return panel;
  }
}
