package com.gitlab.mforoni.jui;

import com.github.mforoni.jbasic.io.JFiles;
import com.google.common.annotations.Beta;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.swing.ImageIcon;

/** @author Foroni Marco */
@Beta
public final class Images {
  // Suppresses default constructor, ensuring non-instantiability.
  private Images() {
    throw new AssertionError();
  }

  /**
   * Load an {@link Image} from a file in resources: the file must be placed in the classpath.
   *
   * @param name
   * @return
   * @see Image
   */
  public static Image newImage(final String name) {
    return newImageIcon(name).getImage();
  }

  /**
   * Creates an {@link ImageIcon} from a file in resources: the file must be placed in the
   * classpath.
   *
   * @param name
   * @return
   * @see ImageIcon
   */
  public static ImageIcon newImageIcon(final String name) {
    try {
      final File file = JFiles.fromResource(name);
      return new ImageIcon(file.getPath());
    } catch (final IOException ex) {
      throw new IllegalStateException(ex);
    }
  }

  /**
   * Returns a copy of the specified {@link BufferedImage}.
   *
   * @param bi a {@code BufferedImage}.
   * @return a copy of the specified {@code BufferedImage}.
   */
  public static BufferedImage deepCopy(final BufferedImage bi) {
    final ColorModel cm = bi.getColorModel();
    final boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
    final WritableRaster raster = bi.copyData(null);
    return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
  }

  /**
   * Returns a scaled new instance of the specified image given the scale factors along the X,Y
   * axes.
   *
   * @param image an {@code Image}.
   * @param sx the factor by which coordinates are scaled along the X axis direction.
   * @param sy the factor by which coordinates are scaled along the Y axis direction.
   * @return a scaled new instance of the specified image.
   */
  public static BufferedImage getScaledInstance(final BufferedImage image, final double sx,
      final double sy) {
    final BufferedImage after =
        new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
    final AffineTransform at = new AffineTransform();
    at.scale(sx, sy);
    final AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
    return scaleOp.filter(image, after);
  }

  /**
   * Returns a scaled new instance of the specified image. The returned {@link BufferedImage} has
   * the required size.
   *
   * @param image an {@code Image}.
   * @param width the required width of the image.
   * @param height the required height of the image.
   * @return a scaled new instance of the specified image.
   */
  public static BufferedImage getScaledInstance(final BufferedImage image, final int width,
      final int height) {
    final double sx = (double) width / image.getWidth();
    final double sy = (double) height / image.getHeight();
    final BufferedImage after = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    final AffineTransform at = new AffineTransform();
    at.scale(sx, sy);
    final AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
    return scaleOp.filter(image, after);
  }
}
