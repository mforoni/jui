package com.gitlab.mforoni.jui;

import com.google.common.annotations.Beta;
import com.google.common.base.Preconditions;
import java.awt.Color;
import java.util.Enumeration;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Beta
public final class GUIManager {
  private static final Logger LOGGER = LoggerFactory.getLogger(GUIManager.class);

  public enum Element {
    OptionPane, Panel, Frame, Button, TextField, ComboBox
  }

  private GUIManager() {
    throw new AssertionError();
  }

  public static void setNimbusLookAndFeel() {
    try {
      UIManager.setLookAndFeel((LookAndFeel) Class
          .forName("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel").newInstance());
    } catch (InstantiationException | IllegalAccessException | ClassNotFoundException
        | UnsupportedLookAndFeelException e) {
      LOGGER.error("Cannot set NimbusLookAndFeel", e);
    }
  }

  /**
   * Setting the same font for all the components.
   *
   * @param f
   * @see UIManager
   */
  public static void setUIFont(final FontUIResource f) {
    final Enumeration<Object> keys = UIManager.getDefaults().keys();
    while (keys.hasMoreElements()) {
      final Object key = keys.nextElement();
      final Object value = UIManager.get(key);
      if (value != null && value instanceof FontUIResource) {
        final FontUIResource original = (FontUIResource) value;
        LOGGER.debug("Change font for object {} from {} to {}", key, original, f);
        UIManager.put(key, new FontUIResource(f));
      }
    }
  }

  public static void setBackgroundColor(final ColorUIResource color) {
    UIManager.put("OptionPane.background", color);
    UIManager.put("Panel.background", color);
    UIManager.put("Frame.background", color);
    UIManager.put("TextField.background", color);
    UIManager.put("ComboBox.background", color);
    UIManager.put("ScrollPane.background", color); // FIXME not working
  }

  public static void setBackgroundColor(final Color color) {
    setBackgroundColor(new ColorUIResource(color));
  }

  public static Color getBackgroundColor(final String key) {
    Preconditions.checkArgument(key.endsWith("background"));
    return UIManager.getColor(key);
  }

  public static void printBackgrounds() {
    final Enumeration<Object> keys = UIManager.getDefaults().keys();
    while (keys.hasMoreElements()) {
      final Object key = keys.nextElement();
      if (key instanceof String) {
        final String s = (String) key;
        if (s.endsWith("background")) {
          final Color c = getBackgroundColor(s);
          LOGGER.info("{} set to {}", s, c);
        }
      }
    }
  }
}
