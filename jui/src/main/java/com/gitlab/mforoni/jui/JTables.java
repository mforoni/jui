package com.gitlab.mforoni.jui;

import com.gitlab.mforoni.jui.model.Header;
import com.google.common.annotations.Beta;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.Map;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

/** @author Foroni Marco */
public final class JTables {
  private JTables() {
    throw new AssertionError();
  }

  /**
   * @param tooltips a map of tooltips, maps a tooltip text by his column name.
   * @return
   */
  public static <T> JTable instance(final Map<T, String> tooltips) {
    return new JTable() {
      /** */
      private static final long serialVersionUID = -1393825793758630877L;

      // workaround for http://bugs.sun.com/view_bug.do?bug_id=4127936
      @Override
      public boolean getScrollableTracksViewportWidth() {
        return getRowCount() == 0 ? super.getScrollableTracksViewportWidth()
            : getPreferredSize().width < getParent().getWidth();
      }

      @Override
      protected JTableHeader createDefaultTableHeader() {
        return new JTableHeader(columnModel) {
          /** */
          private static final long serialVersionUID = -2302009723720804601L;

          @Override
          public String getToolTipText(final MouseEvent e) {
            final java.awt.Point p = e.getPoint();
            final int index = columnModel.getColumnIndexAtX(p.x);
            if (index != -1) {
              final TableColumn column = columnModel.getColumn(index);
              return tooltips.get(column.getHeaderValue());
            } else {
              return null;
            }
          }
        };
      }
    };
  }

  public static JTable instance() {
    return new JTable() {
      /** */
      private static final long serialVersionUID = -5664957349434567812L;

      // workaround for http://bugs.sun.com/view_bug.do?bug_id=4127936
      @Override
      public boolean getScrollableTracksViewportWidth() {
        return getRowCount() == 0 ? super.getScrollableTracksViewportWidth()
            : getPreferredSize().width < getParent().getWidth();
      }
    };
  }

  public static void autofitColumnWidth(final JTable table) {
    for (int column = 0; column < table.getColumnCount(); column++) {
      autofitColumnWidth(table, column);
    }
  }

  public static void autofitColumnWidth(final JTable table, final int column) {
    int width = 0;
    for (int row = 0; row < table.getRowCount(); ++row) {
      final Object cellValue = table.getValueAt(row, column);
      final TableCellRenderer renderer = table.getCellRenderer(row, column);
      final Component comp =
          renderer.getTableCellRendererComponent(table, cellValue, false, false, row, column);
      width = Math.max(width, comp.getPreferredSize().width);
    }
    final TableColumn tc = table.getColumn(table.getColumnName(column));
    width += table.getIntercellSpacing().width * 2;
    tc.setPreferredWidth(width);
    tc.setMinWidth(width);
  }

  public static void setHorizontalAlignment(final JTable table, final Header<?> header) {
    for (int i = 0; i < header.getSize(); i++) {
      table.getColumnModel().getColumn(i)
          .setCellRenderer(TableCellRenderers.withAlignment(header.getAlignment(i)));
    }
  }

  public static void setHorizontalAlignment(final JTable table, final Alignment alignment) {
    for (int column = 0; column < table.getColumnCount(); column++) {
      setHorizontalAlignment(table, column, alignment);
    }
  }

  public static void setHorizontalAlignment(final JTable table, final int column,
      final Alignment alignment) {
    table.getColumnModel().getColumn(column)
        .setCellRenderer(TableCellRenderers.withAlignment(alignment));
  }

  @Beta
  public static Object[] copyRow(final JTable table, final int rowIndex) {
    final TableModel tableModel = table.getModel();
    final int columns = tableModel.getColumnCount();
    final Object[] row = new Object[columns];
    for (int i = 0; i < columns; i++) {
      row[i] = table.getValueAt(rowIndex, i);
    }
    return row;
  }

  public static <E> void setComboBoxColumn(final TableColumn column, final ComboBoxModel<E> model) {
    setComboBoxColumn(column, model, "Click for combo box");
  }

  public static <E> void setComboBoxColumn(final TableColumn column, final ComboBoxModel<E> model,
      final String tooltip) {
    final JComboBox<E> comboBox = new JComboBox<>(model);
    column.setCellEditor(new DefaultCellEditor(comboBox));
    // Set up tool tips for the sport cells.
    final DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
    renderer.setToolTipText(tooltip);
    column.setCellRenderer(renderer);
  }
}
