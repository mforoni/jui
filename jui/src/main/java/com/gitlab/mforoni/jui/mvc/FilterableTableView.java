package com.gitlab.mforoni.jui.mvc;

import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class FilterableTableView {
  private final JTable table = new JTable();
  private final JButton button = new JButton("Filter");
  private final JTextField textField = new JTextField("Insert text here");
  private final JPanel panel = new JPanel();

  public FilterableTableView() {
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    final JPanel controlPanel = new JPanel(new FlowLayout());
    controlPanel.add(textField);
    controlPanel.add(button);
    panel.add(controlPanel);
    // final JPanel tablePanel = new JPanel();
    // tablePanel.setOpaque(true);
    final JScrollPane tableScroll = new JScrollPane(table);
    // final Dimension tablePreferred = tableScroll.getPreferredSize();
    // tableScroll.setPreferredSize(new Dimension(tablePreferred.width, tablePreferred.height / 3));
    // tablePanel.add(tableScroll);
    panel.add(tableScroll);
  }

  public JPanel getPanel() {
    return panel;
  }

  public JButton getFilterButton() {
    return button;
  }

  public JTextField getTextField() {
    return textField;
  }

  public JTable getTable() {
    return table;
  }
}
