package com.gitlab.mforoni.jui.mvc;

import com.gitlab.mforoni.jui.Alignment;

public final class TableOptions {
  public static final TableOptions DEFAULT = new TableOptions(false, false, true, Alignment.CENTER);
  private final boolean autosize;
  private final boolean filterable;
  private final boolean horizontalScroll;
  private final Alignment alignment;

  private TableOptions(final boolean autosize, final boolean filterable,
      final boolean horizontalScroll, final Alignment alignment) {
    super();
    this.autosize = autosize;
    this.filterable = filterable;
    this.horizontalScroll = horizontalScroll;
    this.alignment = alignment;
  }

  public boolean isAutosize() {
    return autosize;
  }

  public boolean isFilterable() {
    return filterable;
  }

  public boolean isHorizontalScroll() {
    return horizontalScroll;
  }

  public Alignment getAlignment() {
    return alignment;
  }

  public static class Builder {
    private boolean autosize;
    private boolean filterable;
    private boolean horizontalScroll;
    private final Alignment alignment;

    public Builder(final Alignment alignment) {
      this.alignment = alignment;
      autosize = false;
      filterable = false;
      horizontalScroll = true;
    }

    public TableOptions.Builder autosize() {
      autosize = true;
      return this;
    }

    public TableOptions.Builder filterable() {
      filterable = true;
      return this;
    }

    public TableOptions.Builder noHorizontalScroll() {
      horizontalScroll = false;
      return this;
    }

    public TableOptions build() {
      return new TableOptions(autosize, filterable, horizontalScroll, alignment);
    }
  }

  @Override
  public String toString() {
    return String.format(
        "TableOptions [autosize=%s, filterable=%s, horizontalScroll=%s, alignment=%s]", autosize,
        filterable, horizontalScroll, alignment);
  }
}
