package com.gitlab.mforoni.jui.mvc.form;

import com.github.mforoni.jbasic.JStrings;
import com.github.mforoni.jbasic.reflect.JFields;
import com.github.mforoni.jbasic.reflect.JMethods;
import com.github.mforoni.jbasic.reflect.JTypes;
import com.google.common.annotations.Beta;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;
import javax.swing.ComboBoxModel;
import javax.swing.JLabel;

/**
 * Use Reflection to automatically create the model from a generic instance of type T.
 *
 * @author marco
 * @param <T>
 */
@Beta
public class InstanceFormModel<T> extends FormModel {
  private final List<InstanceFormElement> elements;
  private final Class<T> type;
  @Nullable
  private T instance = null;

  public InstanceFormModel(final Class<T> type, final List<InstanceFormElement> elements) {
    super(Lists.transform(elements, InstanceFormElement.TO_FORM_ELEMENT));
    this.type = type;
    this.elements = elements;
  }

  public List<InstanceFormElement> getFormElements() {
    return elements;
  }

  public T getInstance() {
    return instance;
  }

  public void setInstance(final T instance) {
    this.instance = instance;
  }

  public T syncAndGetInstance() {
    if (instance == null) {
      instance = JTypes.newInstance(type);
    }
    syncInstance();
    return instance;
  }

  public void setInstanceAndSync(final T instance) {
    this.instance = instance;
    syncUI();
  }

  public void syncUI() {
    syncUI(instance, elements);
  }

  public static <T> void syncUI(final T instance, final List<InstanceFormElement> elements) {
    for (final InstanceFormElement element : elements) {
      final Object instanceValue = JMethods.invokeGetter(element.getGetter(), instance);
      element.getFormElement().setValue(instanceValue);
    }
  }

  public void syncInstance() {
    syncInstance(instance, elements);
  }

  public static <T> void syncInstance(final T instance, final List<InstanceFormElement> elements) {
    for (final InstanceFormElement element : elements) {
      final Optional<Method> setter = element.getSetter();
      if (setter.isPresent()) {
        final Object uiValue = element.getFormElement().getValue();
        JMethods.invokeSetter(setter.get(), instance, uiValue);
      }
    }
  }

  public static final class Builder<T> {
    private final Class<T> type;
    private final List<InstanceFormElement> elements = new ArrayList<>();

    public Builder(final Class<T> type) {
      this.type = type;
    }

    public Builder<T> add(final String getterName, @Nullable final String setterName,
        @Nullable final ComboBoxModel<?> model) {
      final Method getter = JMethods.nonnull(type, getterName);
      Method setter = setterName == null ? null : JMethods.nullable(type, setterName);
      if (setter != null && !Modifier.isPublic(setter.getModifiers())) {
        setter = null;
      }
      final String labelText = JStrings.capitalize(getterName.substring(3));
      final FormElement<?> element =
          model == null ? FormElement.fromType(getter.getReturnType(), labelText, setter != null)
              : new FormElement.FormComboBox<>(new JLabel(labelText), model);
      final InstanceFormElement instanceFormElement =
          new InstanceFormElement(element, getter, Optional.fromNullable(setter));
      elements.add(instanceFormElement);
      return this;
    }

    public Builder<T> add(final String getterName, @Nullable final String setterName) {
      return add(getterName, setterName, null);
    }

    public Builder<T> add(final String getterName, @Nullable final ComboBoxModel<?> model) {
      return add(getterName, null, model);
    }

    public Builder<T> add(final String getterName) {
      return add(getterName, null, null);
    }

    public Builder<T> addFromField(final String fieldName) {
      return add(JMethods.getterName(type, fieldName), JMethods.setterName(fieldName), null);
    }

    public Builder<T> addFromField(final String fieldName, @Nullable final ComboBoxModel<?> model) {
      return add(JMethods.getterName(type, fieldName), JMethods.setterName(fieldName), model);
    }

    public Builder<T> addFromMethod(final String methodName) {
      return add(methodName, null, null);
    }

    public InstanceFormModel<T> build() {
      return new InstanceFormModel<>(type, elements);
    }
  }

  public static <T> InstanceFormModel<T> fromType(final Class<T> type) {
    return fromFields(type, Lists.transform(JFields.fromType(type), JFields.GET_NAME));
  }

  public static <T> InstanceFormModel<T> fromType(final Class<T> type,
      final Predicate<Field> predicate) {
    final List<Field> fields = JFields.fromType(type, predicate);
    return fromFields(type, Lists.newArrayList(Lists.transform(fields, JFields.GET_NAME)));
  }

  public static <T> InstanceFormModel<T> fromType(final Class<T> type, final String fieldName,
      final String... otherFieldNames) {
    return fromFields(type, Lists.asList(fieldName, otherFieldNames));
  }

  private static <T> InstanceFormModel<T> fromFields(final Class<T> type,
      final List<String> fieldNames) {
    final InstanceFormModel.Builder<T> builder = new InstanceFormModel.Builder<>(type);
    for (final String fieldName : fieldNames) {
      builder.addFromField(fieldName);
    }
    return builder.build();
  }
}
