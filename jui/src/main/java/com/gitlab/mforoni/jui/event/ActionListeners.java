package com.gitlab.mforoni.jui.event;

import com.github.mforoni.jbasic.util.JLogger;
import com.gitlab.mforoni.jui.JOptionPanes;
import com.gitlab.mforoni.jui.JOptionPanes.Choice;
import com.gitlab.mforoni.jui.mvc.TableController;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public final class ActionListeners {
  private ActionListeners() {
    throw new AssertionError();
  }

  public static void removeAll(final JButton button) {
    for (final ActionListener al : button.getActionListeners()) {
      button.removeActionListener(al);
    }
  }

  public static ActionListener confirmDeleteRow(final TableController<?> controller) {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final int rowIndex = controller.getView().getSelectedRow();
        if (rowIndex != -1) {
          final Choice choice = JOptionPanes.showConfirmDialog(controller.getView().getPanel(),
              "Are you sure to delete the select row?");
          if (choice == Choice.YES) {
            controller.getTableModel().remove(rowIndex);
          }
        } else {
          JOptionPanes.showInformationDialog(controller.getView().getPanel(),
              "Please select the row to delete");
        }
      }
    };
  }

  public static ActionListener disposeFrame(final JFrame frame) {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        frame.dispose();
      }
    };
  }

  public static ActionListener disposeAllFrames() {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        for (final Frame frame : Frame.getFrames()) {
          frame.dispose();
        }
      }
    };
  }

  public static ActionListener disposeAndRestore(final JFrame dispose, final JFrame restore) {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        dispose.dispose();
        restore.setVisible(true);
      }
    };
  }

  public static ActionListener confirmClosing(final JFrame frame, final String msg,
      final ActionListener action) {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final Choice choice = JOptionPanes.showConfirmDialog(frame, msg);
        if (choice == Choice.YES) {
          JLogger.info(Frame.getFrames());
          action.actionPerformed(e);
          frame.dispose();
        }
      }
    };
  }

  public static ActionListener confirmClosing(final JFrame frame, final String msg) {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final Choice choice = JOptionPanes.showConfirmDialog(frame, msg);
        if (choice == Choice.YES) {
          frame.dispose();
        }
      }
    };
  }
}
