package com.gitlab.mforoni.jui.model;

import com.github.mforoni.jbasic.reflect.JFields;
import com.github.mforoni.jbasic.reflect.JMethods;
import com.gitlab.mforoni.jui.model.Header.Gettable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public final class Headers {
  private Headers() {
    throw new AssertionError();
  }

  public static <T> Header<T> immutable(final Class<T> type) {
    final Header.Builder<T> builder = new Header.Builder<>();
    for (final Field f : JFields.fromType(type)) {
      final Method getter = JMethods.nonnullGetter(type, f.getName());
      final Gettable<T> gettable = new Gettable<T>() {
        @Override
        public Object get(final T obj) {
          return JMethods.invokeGetter(getter, obj);
        }
      };
      builder.add(f.getName(), f.getType(), gettable);
    }
    return builder.build();
  }
}
