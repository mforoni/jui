package com.gitlab.mforoni.jui;

import com.gitlab.mforoni.jui.JOptionPanes.Choice;
import com.gitlab.mforoni.jui.event.WindowListeners;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.JFrame;
import javax.swing.JPanel;

public final class Frame {
  private final JFrame frame;

  public Frame(final JFrame frame) {
    this.frame = frame;
  }

  public void hide() {
    frame.setVisible(false);
  }

  public Choice showConfirmDialog(final String msg) {
    return JOptionPanes.showConfirmDialog(frame, msg);
  }

  public void showInformationDialog(final String msg) {
    JOptionPanes.showInformationDialog(frame, msg);
  }

  public void showWarningDialog(final String msg) {
    JOptionPanes.showWarningDialog(frame, msg);
  }

  public void showErrorDialog(final String msg) {
    JOptionPanes.showErrorDialog(frame, msg);
  }

  public ActionListener disposeAction() {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        frame.dispose();
      }
    };
  }

  public ActionListener closeAndDisposeAction(final AutoCloseable autoCloseable) {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        try {
          autoCloseable.close();
        } catch (final Exception ex) {
          throw new IllegalStateException(ex); // FIXME
        }
        frame.dispose();
      }
    };
  }

  public void addCloseAndDisposeListener(final AutoCloseable autoCloseable) {
    frame.addWindowListener(WindowListeners.closeAndDispose(autoCloseable, frame));
  }

  public void dispose() {
    frame.dispose();
  }

  public void show() {
    frame.pack(); // required after menu construction
    // need to be done after pack() call:
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
  }

  public JFrame get() {
    return frame;
  }

  public static class Builder {
    private final String title;
    private final OnClose onClose;
    private boolean resizable;
    private JPanel north;
    private JPanel west;
    private JPanel center;
    private JPanel east;
    private JPanel south;

    public Builder(final String title) {
      this(title, OnClose.DISPOSE);
    }

    public Builder(final String title, final OnClose onClose) {
      this.title = title;
      this.onClose = onClose;
      resizable = true;
    }

    public Builder notResizable() {
      resizable = false;
      return this;
    }

    public Builder northPanel(final JPanel panel) {
      north = panel;
      return this;
    }

    public Builder westPanel(final JPanel panel) {
      west = panel;
      return this;
    }

    public Builder centerPanel(final JPanel panel) {
      center = panel;
      return this;
    }

    public Builder eastPanel(final JPanel panel) {
      east = panel;
      return this;
    }

    public Builder southPanel(final JPanel panel) {
      south = panel;
      return this;
    }

    public Frame build() {
      final JFrame frame = new JFrame(title);
      frame.setDefaultCloseOperation(onClose.getCode());
      frame.setResizable(resizable);
      addPanels(frame, north, east, center, west, south);
      return new Frame(frame);
    }
  }

  private static void addPanels(@Nonnull final JFrame frame, @Nullable final JPanel north,
      @Nullable final JPanel east, @Nullable final JPanel center, @Nullable final JPanel west,
      @Nullable final JPanel south) {
    final Container contentPane = frame.getContentPane();
    if (north != null) {
      contentPane.add(north, BorderLayout.NORTH);
    }
    if (east != null) {
      contentPane.add(east, BorderLayout.EAST);
    }
    if (center != null) {
      contentPane.add(center, BorderLayout.CENTER);
    }
    if (west != null) {
      contentPane.add(west, BorderLayout.WEST);
    }
    if (south != null) {
      contentPane.add(south, BorderLayout.SOUTH);
    }
  }
}
