package com.gitlab.mforoni.jui.model;

import com.github.mforoni.jbasic.JEnums;
import com.github.mforoni.jbasic.MoreInts;
import com.google.common.primitives.Ints;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

public final class ComboBoxModels {
  private ComboBoxModels() {
    throw new AssertionError();
  }

  public static ComboBoxModel<Integer> fromRange(final int start, final int end) {
    final int[] values = MoreInts.consecutives(start, end);
    return fromCollection(Ints.asList(values), new Integer[values.length]);
  }

  public static <T> ComboBoxModel<T> fromCollection(final Collection<T> collection,
      final T[] array) {
    return new DefaultComboBoxModel<>(collection.toArray(array));
  }

  public static <T> ComboBoxModel<T> fromList(final List<T> list, final Class<T> type) {
    return new DefaultComboBoxModel<>(list.toArray((T[]) Array.newInstance(type, list.size())));
  }

  public static <E extends Enum<E>> ComboBoxModel<E> fromEnum(final Class<E> enumClass) {
    return new DefaultComboBoxModel<>(enumClass.getEnumConstants());
  }

  public static <E extends Enum<E>> ComboBoxModel<String> fromEnumAsStrings(
      final Class<E> enumClass) {
    return new DefaultComboBoxModel<>(JEnums.names(enumClass));
  }
}
