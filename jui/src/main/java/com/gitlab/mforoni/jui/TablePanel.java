package com.gitlab.mforoni.jui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

public final class TablePanel {
  private final JPanel panel;

  private TablePanel(final JPanel panel) {
    this.panel = panel;
  }

  public TablePanel(final JTable table) {
    this(new Builder(table).build().get());
  }

  public JPanel get() {
    return panel;
  }

  public static class Builder {
    private final JPanel panel;

    Builder(final JTable table) {
      panel = new JPanel();
      panel.setLayout(new GridLayout(1, 1));
      add(table);
    }

    public Builder(final Table table) {
      this(table.get());
    }

    private void add(final JTable table) {
      final JScrollPane scrollPane = new JScrollPane(table);
      panel.add(scrollPane, BorderLayout.CENTER);
    }

    public Builder setBorder(final String title) {
      panel.setBorder(new TitledBorder(title));
      return this;
    }

    public TablePanel build() {
      return new TablePanel(panel);
    }
  }
}
