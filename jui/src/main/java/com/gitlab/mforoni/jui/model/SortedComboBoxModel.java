package com.gitlab.mforoni.jui.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;

/**
 * A JComboBox displays the items in the drop down list in the order in which the items where added
 * to the combo box. If you want the items sorted then you can use the
 * {@link Collections#sort(java.util.List)} or {@link Arrays#sort(Object[])} methods on the data
 * before you create the combo box model. But what if you have a dynamic combo box and you want to
 * keep the items in sorted order as new items are added?
 *
 * <p>
 * Well, then you can use the {@code SortedComboBoxModel}. This model extends the {@code
 * DefaultComboBoxModel} and has two additional pieces of functionality built into it:
 *
 * <ul>
 * <li>upon creation of the model, the supplied data will be sorted before the data is added to the
 * model
 * <li>when adding new items to the model, the items will be inserted so as to maintain the sort
 * order
 * </ul>
 *
 * The default sort order will be the natural sort order of the items added to the model. However,
 * you can control this by specifying a custom {@link Comparator} as a parameter of the constructor.
 *
 * <p>
 * Ultimately the {@code SortedComboBoxModel} is a custom model to make sure the items are stored in
 * a sorted order. The default is to sort in the natural order of the item, but a Comparator can be
 * used to customize the sort order.
 *
 * <p>
 * <a href="https://tips4java.wordpress.com/2008/11/11/sorted-combo-box-model/">Link</a>.
 *
 * @author Rob Camick
 */
public final class SortedComboBoxModel<E> extends DefaultComboBoxModel<E> {
  // class SortedComboBoxModel extends DefaultComboBoxModel
  /** */
  private static final long serialVersionUID = -7239221967010365025L;
  private Comparator<E> comparator;

  /** Create an empty model that will use the natural sort order of the item */
  public SortedComboBoxModel() {
    super();
  }

  /**
   * Create an empty model that will use the specified Comparator
   *
   * @param comparator
   */
  public SortedComboBoxModel(final Comparator<E> comparator) {
    super();
    this.comparator = comparator;
  }

  /**
   * Create a model with data and use the nature sort order of the items
   *
   * @param items
   */
  public SortedComboBoxModel(final E items[]) {
    this(items, null);
  }

  /**
   * Create a model with data and use the specified Comparator
   *
   * @param items
   * @param comparator
   */
  public SortedComboBoxModel(final E items[], final Comparator<E> comparator) {
    this.comparator = comparator;
    for (final E item : items) {
      addElement(item);
    }
  }

  /**
   * Create a model with data and use the nature sort order of the items
   *
   * @param items
   */
  public SortedComboBoxModel(final Vector<E> items) {
    this(items, null);
  }

  /** Create a model with data and use the specified Comparator */
  public SortedComboBoxModel(final Vector<E> items, final Comparator<E> comparator) {
    this.comparator = comparator;
    for (final E item : items) {
      addElement(item);
    }
  }

  @Override
  public void addElement(final E element) {
    insertElementAt(element, 0);
  }

  @Override
  public void insertElementAt(final E element, int index) {
    final int size = getSize();
    // Determine where to insert element to keep model in sorted order
    for (index = 0; index < size; index++) {
      if (comparator != null) {
        final E o = getElementAt(index);
        if (comparator.compare(o, element) > 0) {
          break;
        }
      } else {
        final Comparable<E> c = (Comparable<E>) getElementAt(index);
        if (c.compareTo(element) > 0) {
          break;
        }
      }
    }
    super.insertElementAt(element, index);
    // Select an element when it is added to the beginning of the model
    if (index == 0 && element != null) {
      setSelectedItem(element);
    }
  }
}
