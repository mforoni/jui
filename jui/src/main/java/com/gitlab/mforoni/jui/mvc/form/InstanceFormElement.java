package com.gitlab.mforoni.jui.mvc.form;

import com.google.common.annotations.Beta;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import javax.annotation.Nullable;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 * Represents a single form element consisting of a {@link JLabel} and a {@link JComponent} bound to
 * a {@link Field} in order to provide some reflection based capability of auto filling.
 *
 * @author Foroni Marco
 * @see FormElement
 * @see JLabel
 * @see JComponent
 * @see Field
 */
@Beta
public final class InstanceFormElement {
  private final FormElement<?> element;
  private final Method getter;
  private final Optional<Method> setter;
  public static final Function<InstanceFormElement, FormElement<?>> TO_FORM_ELEMENT =
      new Function<InstanceFormElement, FormElement<?>>() {
        @Nullable
        @Override
        public FormElement<?> apply(@Nullable final InstanceFormElement input) {
          return input == null ? null : input.getFormElement();
        }
      };

  public InstanceFormElement(final FormElement<?> element, final Method getter,
      final Optional<Method> setter) {
    this.element = element;
    this.getter = getter;
    this.setter = setter;
  }

  public InstanceFormElement(final FormElement<?> element, final Method getter) {
    this(element, getter, Optional.<Method>absent());
  }

  public InstanceFormElement(final FormElement<?> element, final Method getter,
      final Method setter) {
    this(element, getter, Optional.of(setter));
  }

  public FormElement<?> getFormElement() {
    return element;
  }

  public Method getGetter() {
    return getter;
  }

  public Class<?> getType() {
    return getter.getReturnType();
  }

  public Optional<Method> getSetter() {
    return setter;
  }

  @Override
  public String toString() {
    return "ReflectiveFormElement [element=" + element + ", getter=" + getter + ", setter=" + setter
        + "]";
  }
}
