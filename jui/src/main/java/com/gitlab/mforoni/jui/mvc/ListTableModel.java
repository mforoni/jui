package com.gitlab.mforoni.jui.mvc;

import com.gitlab.mforoni.jui.mvc.Row.State;
import com.google.common.base.Preconditions;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.table.AbstractTableModel;

public class ListTableModel<T> extends AbstractTableModel {
  /** */
  private static final long serialVersionUID = -8056445207037910513L;
  private final TableHeader<T> tableHeader;
  private final Persistence<T> persistence;
  private final List<Row<T>> rows = new ArrayList<>();
  private final List<Row<T>> deleted = new ArrayList<>();

  public ListTableModel(@Nonnull final TableHeader<T> tableHeader) {
    this(tableHeader, new ArrayList<>(), null);
  }

  public ListTableModel(@Nonnull final TableHeader<T> tableHeader, @Nonnull final List<T> list) {
    this(tableHeader, list, null);
  }

  public ListTableModel(@Nonnull final TableHeader<T> tableHeader, @Nonnull final List<T> list,
      @Nullable final Persistence<T> persistence) {
    Preconditions.checkNotNull(tableHeader);
    Preconditions.checkNotNull(list);
    this.tableHeader = tableHeader;
    for (final T instance : list) {
      rows.add(new Row<>(instance, tableHeader));
    }
    this.persistence = persistence;
  }

  public List<T> getList() {
    final List<T> list = new ArrayList<>();
    for (final Row<T> row : rows) {
      list.add(row.getInstance());
    }
    return list;
  }

  public void setList(final List<T> list) {
    if (isModified()) {
      throw new IllegalStateException();
    }
    resetList(list);
  }

  public boolean isModified() {
    for (final Row<T> row : rows) {
      if (row.getState() != State.UNCHANGED) {
        return true;
      }
    }
    return false;
  }

  void resetList(final List<T> list) {
    rows.clear();
    for (final T instance : list) {
      rows.add(new Row<>(instance, tableHeader));
    }
    fireTableDataChanged();
  }

  public void add(@Nonnull final T instance) {
    rows.add(new Row<>(instance, tableHeader));
    fireTableDataChanged();
  }

  // requires that T implements equals
  @Deprecated
  private static <T> Row<T> find(final List<Row<T>> list, final T instance) {
    final List<Row<T>> matching = new ArrayList<>();
    for (final Row<T> row : list) {
      if (Objects.equals(row.getInstance(), instance)) {
        matching.add(row);
      }
    }
    if (matching.size() == 1) {
      return matching.get(0);
    } else {
      throw new AssertionError();
    }
  }

  public void remove(@Nonnull final T instance) {
    remove(find(rows, instance));
  }

  public void remove(final int rowIndex) {
    remove(getRow(rowIndex));
  }

  private void remove(Row<T> row) {
    switch (row.getState()) {
      case NEW:
        row.delete();
        rows.remove(row);
        row = null;
        fireTableDataChanged();
        break;
      case UNCHANGED:
        row.delete();
        rows.remove(row);
        deleted.add(row);
        fireTableDataChanged();
        break;
      case CHANGED:
        row.delete();
        rows.remove(row);
        deleted.add(row);
        fireTableDataChanged();
        break;
      case DELETED:
        throw new IllegalStateException();
    }
  }

  @Nonnull
  Row<T> getRow(final int rowIndex) {
    return rows.get(rowIndex);
  }

  @Nonnull
  public T getInstance(final int rowIndex) {
    return getRow(rowIndex).getInstance();
  }

  @Override
  public int getRowCount() {
    return rows.size();
  }

  public boolean isEditable() {
    return tableHeader.isEditable();
  }

  public boolean isEditble(final int columnIndex) {
    return tableHeader.isEditable(columnIndex);
  }

  @Override
  public boolean isCellEditable(final int rowIndex, final int columnIndex) {
    return tableHeader.isEditable(columnIndex);
  }

  @Override
  public Object getValueAt(final int rowIndex, final int columnIndex) {
    final Row<T> row = getRow(rowIndex);
    return row.getValueAt(columnIndex);
  }

  @Override
  public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
    if (!tableHeader.isEditable(columnIndex)) {
      throw new AssertionError();
    }
    final Row<T> row = getRow(rowIndex);
    row.setValueAt(columnIndex, value);
  }

  @Override
  public String getColumnName(final int columnIndex) {
    return tableHeader.getColumnName(columnIndex);
  }

  @Override
  public Class<?> getColumnClass(final int columnIndex) {
    return tableHeader.getColumnClass(columnIndex);
  }

  @Override
  public int getColumnCount() {
    return tableHeader.getColumnCount();
  }

  public ActionListener saveActionListener() {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        Preconditions.checkState(persistence != null);
        final List<Row<T>> list = new ArrayList<>(rows);
        list.addAll(deleted);
        persistence.save(list);
      }
    };
  }
}
