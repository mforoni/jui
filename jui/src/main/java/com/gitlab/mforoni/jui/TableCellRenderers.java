package com.gitlab.mforoni.jui;

import javax.swing.table.DefaultTableCellRenderer;

public final class TableCellRenderers {
  private TableCellRenderers() {
    throw new AssertionError();
  }

  public static DefaultTableCellRenderer centerAlignment() {
    return withAlignment(Alignment.CENTER);
  }

  public static DefaultTableCellRenderer withAlignment(final Alignment alignment) {
    final DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
    renderer.setHorizontalAlignment(alignment.getSwingConstantValue());
    return renderer;
  }
}
