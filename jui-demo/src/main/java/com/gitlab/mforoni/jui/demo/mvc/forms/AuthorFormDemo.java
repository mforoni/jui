package com.gitlab.mforoni.jui.demo.mvc.forms;

import javax.swing.SwingUtilities;
import com.github.mforoni.jsupport.Author;
import com.github.mforoni.jsupport.Authors;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.mvc.form.FormController;
import com.gitlab.mforoni.jui.mvc.form.InstanceFormModel;

final class AuthorFormDemo {
  private AuthorFormDemo() {}

  private static void createAndShowGUI() {
    final InstanceFormModel<Author> model = new InstanceFormModel.Builder<>(Author.class)
        .addFromField("firstName").addFromField("lastName").addFromField("dateBirth")
        .addFromField("gender").addFromField("email").addFromField("books").build();
    final FormController<InstanceFormModel<Author>> controller = new FormController<>(model);
    final Frame frame =
        new Frame.Builder("Author Form Demo").centerPanel(controller.getView().getJPanel()).build();
    model.setInstance(Authors.ANNIE_DILLARD);
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        try {
          createAndShowGUI();
        } catch (final Throwable t) {
          t.printStackTrace();
        }
      }
    });
  }
}
