package com.gitlab.mforoni.jui.demo;

import com.gitlab.mforoni.jui.GUIManager;
import com.gitlab.mforoni.jui.MultiSelect;
import com.google.common.base.Joiner;
import java.util.Arrays;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

final class MultiSelectDemo {
  private static final String[] ITEMS = {"one", "two", "three", "four"};

  private MultiSelectDemo() {}

  private static void createAndShowGUI() {
    final JFrame frame = new JFrame();
    final JPanel panel = new JPanel();
    final MultiSelect<String> multiSelect = new MultiSelect<>();
    multiSelect.setItems(Arrays.asList(ITEMS));
    final JButton button = new JButton("Print selected");
    button.addActionListener(e -> {
      final List<String> selectedItems = multiSelect.getSelectedItems();
      System.out.println(Joiner.on(", ").join(selectedItems));
    });
    panel.add(multiSelect.get());
    panel.add(button);
    frame.getContentPane().add(panel);
    frame.pack();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(() -> {
      GUIManager.setNimbusLookAndFeel();
      createAndShowGUI();
    });
  }
}
