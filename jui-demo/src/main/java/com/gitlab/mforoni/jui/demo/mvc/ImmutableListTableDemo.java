package com.gitlab.mforoni.jui.demo.mvc;

import com.github.mforoni.jsupport.ImmutablePerson;
import com.github.mforoni.jsupport.ImmutablePersons;
import com.github.mforoni.jsupport.Person.Gender;
import com.gitlab.mforoni.jui.Alignment;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.mvc.ListTableModel;
import com.gitlab.mforoni.jui.mvc.TableController;
import com.gitlab.mforoni.jui.mvc.TableHeader;
import com.gitlab.mforoni.jui.mvc.TableOptions;
import com.gitlab.mforoni.jui.mvc.TableView;
import java.util.List;
import javax.swing.SwingUtilities;
import org.joda.time.LocalDate;

final class ImmutableListTableDemo {
  private ImmutableListTableDemo() {}

  private static void createAndShowGUI() {
    final List<ImmutablePerson> persons = ImmutablePersons.ARRAY_LIST;
    final TableView view =
        new TableView(new TableOptions.Builder(Alignment.LEFT).filterable().build());
    final ListTableModel<ImmutablePerson> tableModel =
        new ListTableModel<>(new PersonTableHeader());
    final TableController<ImmutablePerson> controller = new TableController<>(view, tableModel);
    controller.setList(persons);
    final Frame frame =
        new Frame.Builder("Immutable Table Demo").centerPanel(view.getPanel()).build();
    frame.show();
  }

  private static final class PersonTableHeader implements TableHeader<ImmutablePerson> {
    private static final String[] HEADER =
        {"LAST_NAME", "FIRST_NAME", "GENDER", "DATE_BIRTH", "EMAIL"};
    private static final Class<?>[] CLASSES =
        {String.class, String.class, Gender.class, LocalDate.class, String.class};

    @Override
    public Object getValue(final ImmutablePerson instance, final int columnIndex) {
      switch (columnIndex) {
        case 0:
          return instance.getLastName();
        case 1:
          return instance.getFirstName();
        case 2:
          return instance.getGender();
        case 3:
          return instance.getDateBirth();
        case 4:
          return instance.getEmail();
        default:
          throw new AssertionError();
      }
    }

    @Override
    public int getColumnCount() {
      return HEADER.length;
    }

    @Override
    public String getColumnName(final int column) {
      return HEADER[column];
    }

    @Override
    public Class<?> getColumnClass(final int column) {
      return CLASSES[column];
    }
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
