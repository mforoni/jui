package com.gitlab.mforoni.jui.demo.mvc;

import com.gitlab.mforoni.jui.Alignment;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.GUIManager;
import com.gitlab.mforoni.jui.event.ActionListeners;
import com.gitlab.mforoni.jui.mvc.DisplayableTableHeader;
import com.gitlab.mforoni.jui.mvc.ListTableModel;
import com.gitlab.mforoni.jui.mvc.TableController;
import com.gitlab.mforoni.jui.mvc.TableOptions;
import com.gitlab.mforoni.jui.mvc.TableView;
import javax.swing.SwingUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class DisplayableMutableTableDemo {
  private static final Logger LOGGER = LoggerFactory.getLogger(DisplayableMutableTableDemo.class);
  private static final int DEFAULT_SIZE = 15;

  private DisplayableMutableTableDemo() {}

  static int getNextId(final ListTableModel<Player> model) {
    int max = -1;
    for (final Player p : model.getList()) {
      if (p.getId() > max) {
        max = p.getId();
      }
    }
    return max + 1;
  }

  private static void createAndShowGUI() {
    final TableOptions tableOptions =
        new TableOptions.Builder(Alignment.CENTER).autosize().filterable().build();
    final TableView view = new TableView(tableOptions);
    final DisplayableTableHeader<Player> header = DisplayableTableHeader.editable(Player.class);
    LOGGER.info("{}", header);
    final ListTableModel<Player> tableModel = new ListTableModel<>(header);
    final TableController<Player> controller = new TableController<>(view, tableModel);
    final Frame frame =
        new Frame.Builder("Displayable Mutable Table Demo").centerPanel(view.getPanel()).build();
    controller.addInsertRowAction(e -> {
      controller.getTableModel().add(new Player(getNextId(tableModel), "", "", "", null, null, ""));
    });
    controller.addDeleteRowAction(ActionListeners.confirmDeleteRow(controller));
    controller.setList(Players.mutableList(DEFAULT_SIZE));
    frame.show();
  }

  public static void main(final String[] args) {
    GUIManager.setNimbusLookAndFeel();
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
