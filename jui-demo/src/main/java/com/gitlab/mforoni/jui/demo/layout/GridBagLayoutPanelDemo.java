package com.gitlab.mforoni.jui.demo.layout;

import com.gitlab.mforoni.jui.Borders;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.JLabels;
import com.gitlab.mforoni.jui.layout.FlowLayoutPanel;
import com.gitlab.mforoni.jui.layout.GridBagLayoutPanel;
import com.gitlab.mforoni.jui.layout.GridBagLayoutPanel.DisplayConstraints;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

final class GridBagLayoutPanelDemo {
  private static final double[] WEIGHTS_X = new double[] {0.75, 0.25};

  private GridBagLayoutPanelDemo() {} // Preventing instantiation

  private static void createAndShowGUI() {
    final JPanel southPanel =
        new FlowLayoutPanel().add(new JButton("Ok"), new JButton("Cancel")).build();
    final Frame frame = new Frame.Builder("GridBagLayout Demo").centerPanel(centerPanel())
        .southPanel(southPanel).build();
    frame.show();
  }

  private static JPanel centerPanel() {
    final DisplayConstraints displayer = new DisplayConstraints() {
      @Override
      public Double getComponentWeightX(final int col) {
        return WEIGHTS_X[col];
      }

      @Override
      public Integer getComponentFillType(final int col) {
        return null;
      }

      @Override
      public Integer getComponentAnchor(final int col) {
        return null;
      }
    };
    return new GridBagLayoutPanel().setBorder("Title")
        .addRow(displayer, new JLabel("Very long text let see if it can be handled well:"),
            JLabels.bordered("Some text", Borders.LOWERED_ETCHED_BORDER))
        .addRow(displayer, new JLabel("Short text:"),
            JLabels.bordered("Some text", Borders.RAISED_ETCHED_BORDER))
        .build();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
