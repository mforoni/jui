package com.gitlab.mforoni.jui.demo.mvc;

import com.github.mforoni.jsupport.Person.Gender;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;
import org.fluttercode.datafactory.impl.DataFactory;
import org.joda.time.LocalDate;

final class Players {
  private Players() {
    throw new AssertionError();
  }

  public static ImmutableList<Player> immutableList(final int size) {
    final DataFactory df = new DataFactory();
    final Player[] players = new Player[size];
    for (int i = 0; i < size; i++) {
      players[i] = new Player(i + 1, df.getFirstName(), df.getLastName(), df.getName(), Gender.MALE,
          new LocalDate(df.getBirthDate()), df.getEmailAddress());
    }
    return ImmutableList.copyOf(players);
  }

  public static List<Player> mutableList(final int size) {
    final DataFactory df = new DataFactory();
    final List<Player> players = new ArrayList<>();
    for (int i = 0; i < size; i++) {
      players.add(new Player(i + 1, df.getFirstName(), df.getLastName(), df.getName(), Gender.MALE,
          new LocalDate(df.getBirthDate()), df.getEmailAddress()));
    }
    return players;
  }
}
