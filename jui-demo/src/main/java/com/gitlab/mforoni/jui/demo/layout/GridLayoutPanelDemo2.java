package com.gitlab.mforoni.jui.demo.layout;

import com.gitlab.mforoni.jui.Borders;
import com.gitlab.mforoni.jui.Fonts;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.JLabels;
import com.gitlab.mforoni.jui.layout.FlowLayoutPanel;
import com.gitlab.mforoni.jui.layout.GridLayoutPanel;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

final class GridLayoutPanelDemo2 {
  private GridLayoutPanelDemo2() {} // Preventing instantiation

  private static final String[] NAMES =
      new String[] {"Ester", "Jordi", "Jordina", "Jorge", "Sergi"};

  private static void createAndShowGUI() {
    final JPanel northPanel =
        new FlowLayoutPanel(new JLabel("Testing Grid Layout with JScrollPane")).build();
    final JPanel centerPanel = new GridLayoutPanel(2, 2).border(Borders.EMPTY_BORDER)
        .add(new JLabel("Very long text let see if it can be handled well:"))
        .add(JLabels.bordered("Short text", Borders.RAISED_BEVEL_BORDER))
        .add(new JLabel("Short text:")).add(new JScrollPane(new JList<>(NAMES))).build();
    final JPanel southPanel =
        new FlowLayoutPanel().add(new JButton("Ok"), new JButton("Cancel")).build();
    final Frame frame = new Frame.Builder("Grid Layout Demo 2").northPanel(northPanel)
        .southPanel(southPanel).centerPanel(centerPanel).build();
    frame.get().setBackground(Color.BLACK); // Not working
    frame.get().setFont(Fonts.TAHOMA_16);
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        createAndShowGUI();
      }
    });
  }
}
