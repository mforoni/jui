package com.gitlab.mforoni.jui.demo.mvc.forms;

import com.github.mforoni.jbasic.reflect.JFields;
import com.github.mforoni.jsupport.ImmutablePerson;
import com.github.mforoni.jsupport.ImmutablePersons;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.event.ActionListeners;
import com.gitlab.mforoni.jui.mvc.form.FormController;
import com.gitlab.mforoni.jui.mvc.form.InstanceFormModel;
import javax.swing.SwingUtilities;

final class ImmutablePersonInstanceFormDemo {
  private ImmutablePersonInstanceFormDemo() {}

  private static void createAndShowGUI() {
    final InstanceFormModel<ImmutablePerson> model =
        InstanceFormModel.fromType(ImmutablePerson.class, JFields.IS_NOT_STATIC);
    final FormController<InstanceFormModel<ImmutablePerson>> controller =
        new FormController<>(model);
    final Frame frame = new Frame.Builder("Immutable Person Form Demo")
        .centerPanel(controller.getView().getJPanel()).build();
    controller.getView().getButton().setText("Close");
    controller.addButtonActionListener(ActionListeners.disposeFrame(frame.get()));
    model.setInstanceAndSync(ImmutablePersons.MARCO_NERI);
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
