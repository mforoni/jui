package com.gitlab.mforoni.jui.demo.mvc.forms;

import com.github.mforoni.jbasic.reflect.JFields;
import com.github.mforoni.jsupport.Employee;
import com.github.mforoni.jsupport.Employees;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.OnClose;
import com.gitlab.mforoni.jui.event.WindowListeners;
import com.gitlab.mforoni.jui.mvc.form.FormController;
import com.gitlab.mforoni.jui.mvc.form.InstanceFormModel;
import javax.swing.SwingUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class EmployeeInstanceFormDemo {
  private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeInstanceFormDemo.class);

  private EmployeeInstanceFormDemo() {}

  private static void createAndShowGUI(final Employee employee) {
    final InstanceFormModel<Employee> model =
        InstanceFormModel.fromType(Employee.class, JFields.IS_NOT_STATIC);
    final FormController<InstanceFormModel<Employee>> controller = new FormController<>(model);
    final Frame frame = new Frame.Builder("Employee Form Demo", OnClose.DO_NOTHING)
        .centerPanel(controller.getView().getJPanel()).build();
    frame.get().addWindowListener(WindowListeners.disposeFrame(frame.get()));
    controller.addButtonActionListener(e -> {
      LOGGER.info("{}", model.syncAndGetInstance());
    });
    model.setInstance(employee);
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI(Employees.newRandomList(1).iterator().next());
      }
    });
  }
}
