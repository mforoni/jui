package com.gitlab.mforoni.jui.demo.layout;

import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.layout.BoxLayoutPanel;
import com.gitlab.mforoni.jui.layout.BoxLayoutPanel.Axis;
import com.gitlab.mforoni.jui.layout.FlowLayoutPanel;
import com.gitlab.mforoni.jui.layout.GroupLayoutPanel;
import com.google.common.collect.ImmutableList;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

final class GroupLayoutPanelDemo2 {
  private GroupLayoutPanelDemo2() {}

  private static void createAndShowGUI() {
    JFrame.setDefaultLookAndFeelDecorated(true);
    final JPanel formPanel = formPanel();
    final JPanel buttonsPanel =
        new FlowLayoutPanel().add(new JButton("Ok"), new JButton("Cancel")).build();
    final JPanel panel = new BoxLayoutPanel(Axis.Y).add(formPanel).add(buttonsPanel).build();
    final Frame frame = new Frame.Builder("GroupLayout Demo 2").northPanel(panel).build();
    frame.show();
  }

  private static JPanel formPanel() {
    final JLabel lblName = new JLabel("Name");
    final JLabel lblMail = new JLabel("Mail");
    final JLabel lblDateBirth = new JLabel("Date birth");
    final JLabel lblBooks = new JLabel("Books");
    final JComponent txtName = new JTextField(), txtMail = new JTextField(),
        txtDateBirth = new JTextField(), listBooks = new JScrollPane(new JList<String>());
    final ImmutableList<JLabel> labels = ImmutableList.of(lblName, lblMail, lblDateBirth, lblBooks);
    final ImmutableList<JComponent> components =
        ImmutableList.of(txtName, txtMail, txtDateBirth, listBooks);
    final GroupLayoutPanel groupLayoutPanel = new GroupLayoutPanel(labels, components);
    return groupLayoutPanel.build();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
