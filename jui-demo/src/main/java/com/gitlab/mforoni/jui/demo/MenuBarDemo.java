package com.gitlab.mforoni.jui.demo;

import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.MenuBar;
import com.gitlab.mforoni.jui.MenuBar.Menu;
import com.gitlab.mforoni.jui.OnClose;
import com.gitlab.mforoni.jui.event.ActionListeners;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

final class MenuBarDemo {
  MenuBarDemo() {}

  private static void createAndShowGUI() {
    final JPanel centerPanel = new JPanel();
    centerPanel.add(new JLabel("Description"));
    final Frame frame =
        new Frame.Builder("Menu Bar Demo", OnClose.DISPOSE).centerPanel(centerPanel).build();
    final Menu file = new Menu("File").item("Exit", "Quit application");
    final Menu edit = new Menu("Edit").item("Cut").item("Copy").item("Paste");
    final MenuBar menuBar = new MenuBar(file).add(edit);
    final JMenuItem menuItem = menuBar.getJMenuItem("File->Exit");
    menuItem.addActionListener(
        ActionListeners.confirmClosing(frame.get(), "Are you sure to exit application?"));
    frame.get().setJMenuBar(menuBar.toJMenuBar());
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        try {
          createAndShowGUI();
        } catch (final Exception ex) {
          ex.printStackTrace();
        }
      }
    });
  }
}
