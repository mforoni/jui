package com.gitlab.mforoni.jui.demo.layout;

import com.gitlab.mforoni.jui.Colors;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.GUIManager;
import com.gitlab.mforoni.jui.JComboBoxes;
import com.gitlab.mforoni.jui.JLabels;
import com.gitlab.mforoni.jui.layout.FlowLayoutPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

final class FlowLayoutPanelDemo2 {
  private static final String[] NAMES =
      new String[] {"Ester", "Jordi", "Jordina", "Jorge", "Sergi"};

  private FlowLayoutPanelDemo2() {} // Preventing instantiation

  private static void createAndShowGUI() {
    final JComboBox<String> comboBox = JComboBoxes.of(NAMES);
    final JPanel panel = new FlowLayoutPanel().titledBorder("Persons")
        .add(JLabels.bordered("Select a person:"), comboBox, new JButton("Ok")).build();
    final Frame frame = new Frame.Builder("FlowLayout Demo 2").northPanel(panel).build();
    SwingUtilities.updateComponentTreeUI(frame.get());
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        GUIManager.setBackgroundColor(Colors.LIGHT_AZURE);
        GUIManager.printBackgrounds();
        createAndShowGUI();
      }
    });
  }
}
