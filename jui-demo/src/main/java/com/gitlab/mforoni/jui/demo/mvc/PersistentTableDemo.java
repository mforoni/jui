package com.gitlab.mforoni.jui.demo.mvc;

import static com.gitlab.mforoni.jui.demo.mvc.DisplayableMutableTableDemo.getNextId;
import com.github.mforoni.jsupport.Person.Gender;
import com.gitlab.mforoni.jui.Alignment;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.MenuBar;
import com.gitlab.mforoni.jui.MenuBar.Menu;
import com.gitlab.mforoni.jui.event.ActionListeners;
import com.gitlab.mforoni.jui.mvc.DisplayableTableHeader;
import com.gitlab.mforoni.jui.mvc.ListTableModel;
import com.gitlab.mforoni.jui.mvc.Persistence;
import com.gitlab.mforoni.jui.mvc.TableController;
import com.gitlab.mforoni.jui.mvc.TableOptions;
import com.gitlab.mforoni.jui.mvc.TableView;
import java.util.Collections;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import org.joda.time.LocalDate;

final class PersistentTableDemo {
  private PersistentTableDemo() {}

  private static void createAndShowGUI() {
    final TableView view =
        new TableView(new TableOptions.Builder(Alignment.CENTER).filterable().autosize().build());
    final DisplayableTableHeader<Player> header = DisplayableTableHeader.editable(Player.class);
    final ListTableModel<Player> tableModel =
        new ListTableModel<>(header, Collections.emptyList(), new Persistence<Player>());
    final TableController<Player> controller = new TableController<>(view, tableModel);
    final Frame frame =
        new Frame.Builder("Persistent Table Demo").centerPanel(view.getPanel()).build();
    controller.addDeleteRowAction(ActionListeners.confirmDeleteRow(controller));
    controller.addInsertRowAction(e -> tableModel.add(generateNewPlayer(tableModel)));
    controller.setList(Players.mutableList(20));
    final Menu fileMenu =
        new Menu("File").item("Save", controller.getTableModel().saveActionListener());
    final MenuBar menuBar = new MenuBar(fileMenu);
    frame.get().setJMenuBar(menuBar.toJMenuBar());
    frame.show();
  }

  private static Player generateNewPlayer(final ListTableModel<Player> tableModel) {
    return new Player(getNextId(tableModel), "First Name", "Last Name", "Nickname", Gender.MALE,
        new LocalDate(), "email");
  }

  public static void main(final String[] args) {
    JFrame.setDefaultLookAndFeelDecorated(true);
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
