package com.gitlab.mforoni.jui.demo.layout;

import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.JComboBoxes;
import com.gitlab.mforoni.jui.layout.BoxLayoutPanel;
import com.gitlab.mforoni.jui.layout.BoxLayoutPanel.Axis;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

final class BoxLayoutPanelDemo {
  private BoxLayoutPanelDemo() {} // Preventing instantiation

  private static final String[] NAMES =
      new String[] {"Ester", "Jordi", "Jordina", "Jorge", "Sergi"};

  private static void createAndShowGUI() {
    final JPanel northPanel =
        new BoxLayoutPanel(Axis.X).titledBorder("Title").add(new JLabel("A label")).build();
    final JComboBox<String> comboBox = JComboBoxes.of(NAMES);
    final JPanel centerPanel = new BoxLayoutPanel(Axis.X).titledBorder("Persons")
        .add(new JLabel("Select a person: "), comboBox).build();
    final JPanel southPanel =
        new BoxLayoutPanel(Axis.X).add(new JButton("Ok"), new JButton("Cancel")).build();
    final Frame frame = new Frame.Builder("BoxLayout Demo").northPanel(northPanel)
        .centerPanel(centerPanel).southPanel(southPanel).build();
    SwingUtilities.updateComponentTreeUI(frame.get());
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
