package com.gitlab.mforoni.jui.demo.mvc.forms;

import com.github.mforoni.jsupport.ImmutablePerson;
import com.github.mforoni.jsupport.ImmutablePersons;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.event.ActionListeners;
import com.gitlab.mforoni.jui.mvc.form.FormController;
import com.gitlab.mforoni.jui.mvc.form.FormElement.ComponentType;
import com.gitlab.mforoni.jui.mvc.form.FormModel;
import javax.swing.SwingUtilities;

final class ImmutablePersonFormDemo {
  private ImmutablePersonFormDemo() {}

  private static void createAndShowGUI() {
    final ImmutablePerson person = ImmutablePersons.MARCO_NERI;
    final FormModel model = new FormModel.Builder().add("First Name", ComponentType.JLABEL)
        .add("Last Name", ComponentType.JLABEL).build();
    model.getElement("First Name").setValue(person.getFirstName());
    model.getElement("Last Name").setValue(person.getLastName());
    final FormController<FormModel> controller = new FormController<>(model);
    final Frame frame = new Frame.Builder("Immutable Person Form Demo")
        .centerPanel(controller.getView().getJPanel()).build();
    controller.getView().getButton().setText("Close");
    controller.addButtonActionListener(ActionListeners.disposeFrame(frame.get()));
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
