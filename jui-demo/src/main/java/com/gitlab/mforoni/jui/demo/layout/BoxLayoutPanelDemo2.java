package com.gitlab.mforoni.jui.demo.layout;

import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.JComboBoxes;
import com.gitlab.mforoni.jui.layout.BoxLayoutPanel;
import com.gitlab.mforoni.jui.layout.BoxLayoutPanel.Axis;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

final class BoxLayoutPanelDemo2 {
  private BoxLayoutPanelDemo2() {} // Preventing instantiation

  private static final String[] NAMES =
      new String[] {"Ester", "Jordi", "Jordina", "Jorge", "Sergi"};

  private static void createAndShowGUI() {
    final JComboBox<String> comboBox = JComboBoxes.of(NAMES);
    final JPanel selectPanel = new BoxLayoutPanel(Axis.X).titledBorder("Persons")
        .add(new JLabel("Select a person: "), comboBox).build();
    final JPanel buttonPanels =
        new BoxLayoutPanel(Axis.X).add(new JButton("Ok"), new JButton("Cancel")).build();
    final JPanel panel = new BoxLayoutPanel(Axis.Y).titledBorder("BoxLayout").add(selectPanel)
        .add(buttonPanels).build();
    final Frame frame = new Frame.Builder("BoxLayout Demo 2").northPanel(panel).build();
    SwingUtilities.updateComponentTreeUI(frame.get());
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
