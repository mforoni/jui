package com.gitlab.mforoni.jui.demo.mvc;

import com.github.mforoni.jsupport.Person;
import java.util.List;
import javax.swing.table.DefaultTableModel;

final class Persons {
  private Persons() {
    throw new AssertionError();
  }

  private static final String[] HEADER =
      {"FIRST NAME", "LAST NAME", "GENDER", "DATE BIRTH", "EMAIL"};

  public static <T extends Person> DefaultTableModel defaultTableModel(final List<T> persons) {
    final Object[][] data = new Object[persons.size()][5];
    for (int r = 0; r < persons.size(); r++) {
      final Person p = persons.get(r);
      data[r][0] = p.getFirstName();
      data[r][1] = p.getLastName();
      data[r][2] = p.getGender();
      data[r][3] = p.getDateBirth();
      data[r][4] = p.getEmail();
    }
    return new DefaultTableModel(data, HEADER);
  }
}
