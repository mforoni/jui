package com.gitlab.mforoni.jui.demo.mvc;

import com.github.mforoni.jsupport.Person.Gender;
import com.gitlab.mforoni.jui.mvc.DisplayAs;
import java.io.Serializable;
import org.joda.time.LocalDate;

public final class Player implements Serializable {
  /** */
  private static final long serialVersionUID = 1372782142728113259L;
  @DisplayAs(index = 0, value = "ID")
  private int id;
  @DisplayAs(index = 1, value = "FIRST NAME")
  private String firstName;
  @DisplayAs(index = 2, value = "LAST NAME")
  private String lastName;
  @DisplayAs(index = 3, value = "NICKNAME")
  private String nickname;
  @DisplayAs(index = 4, value = "GENDER")
  private Gender gender;
  @DisplayAs(index = 5, value = "BIRTH DATE")
  private LocalDate birthDate;
  @DisplayAs(index = 6, value = "EMAIL")
  private String email;

  public Player(final int id, final String firstName, final String lastName, final String nickname,
      final Gender gender, final LocalDate birthDate, final String email) {
    super();
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.nickname = nickname;
    this.gender = gender;
    this.birthDate = birthDate;
    this.email = email;
  }

  public int getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getNickname() {
    return nickname;
  }

  public Gender getGender() {
    return gender;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public String getEmail() {
    return email;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public void setNickname(final String nickname) {
    this.nickname = nickname;
  }

  public void setGender(final Gender gender) {
    this.gender = gender;
  }

  public void setBirthDate(final LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  @Override
  public String toString() {
    return firstName + ", " + lastName;
  }
}
