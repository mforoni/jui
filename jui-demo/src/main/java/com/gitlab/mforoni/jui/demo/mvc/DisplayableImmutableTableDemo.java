package com.gitlab.mforoni.jui.demo.mvc;

import com.gitlab.mforoni.jui.Alignment;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.mvc.DisplayableTableHeader;
import com.gitlab.mforoni.jui.mvc.ListTableModel;
import com.gitlab.mforoni.jui.mvc.TableController;
import com.gitlab.mforoni.jui.mvc.TableOptions;
import com.gitlab.mforoni.jui.mvc.TableView;
import javax.swing.SwingUtilities;

final class DisplayableImmutableTableDemo {
  private static final int DEFAULT_SIZE = 15;

  private DisplayableImmutableTableDemo() {}

  private static void createAndShowGUI() {
    final TableView view =
        new TableView(new TableOptions.Builder(Alignment.CENTER).autosize().filterable().build());
    final DisplayableTableHeader<Player> header = DisplayableTableHeader.notEditable(Player.class);
    final ListTableModel<Player> model = new ListTableModel<>(header);
    final TableController<Player> controller = new TableController<>(view, model);
    controller.setList(Players.immutableList(DEFAULT_SIZE));
    final Frame frame =
        new Frame.Builder("Displayable Immutable Table Demo").centerPanel(view.getPanel()).build();
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
