package com.gitlab.mforoni.jui.demo.mvc.forms;

import com.github.mforoni.jbasic.reflect.JFields;
import com.github.mforoni.jsupport.beans.PersonBean;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.mvc.form.FormController;
import com.gitlab.mforoni.jui.mvc.form.InstanceFormModel;
import javax.swing.SwingUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class PersonBeanFormDemo {
  private static final Logger LOGGER = LoggerFactory.getLogger(PersonBeanFormDemo.class);

  private PersonBeanFormDemo() {}

  private static void createAndShowGUI() {
    final InstanceFormModel<PersonBean> model =
        InstanceFormModel.fromType(PersonBean.class, JFields.IS_NOT_STATIC);
    model.setInstance(new PersonBean());
    final FormController<InstanceFormModel<PersonBean>> controller = new FormController<>(model);
    final Frame frame =
        new Frame.Builder("Person Bean Form").centerPanel(controller.getView().getJPanel()).build();
    controller.addButtonActionListener(e -> {
      LOGGER.info("Confirm button pressed: created new PersonBean {}", model.getInstance());
    });
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
