package com.gitlab.mforoni.jui.demo;

import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.JComboBoxes;
import com.gitlab.mforoni.jui.OnClose;
import com.gitlab.mforoni.jui.layout.FlowLayoutPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * Requires SwingX API
 *
 * @author Foroni Marco
 */
final class AutocompleteComboBoxDemo {
  private AutocompleteComboBoxDemo() {} // Preventing instantiation

  private static void createAndShowGUI() {
    final Frame frame = new Frame.Builder("Autocomplete ComboBox Demo", OnClose.DISPOSE)
        .centerPanel(buildCenterPanel()).southPanel(buildSouthPanel()).build();
    frame.show();
  }

  private static JPanel buildCenterPanel() {
    final JLabel label = new JLabel("Pick a person");
    final JComboBox<String> comboBox =
        JComboBoxes.of(new String[] {"Ester", "Jordi", "Jordina", "Jorge", "Sergi"});
    return new FlowLayoutPanel().titledBorder("Persons").add(label, comboBox).build();
  }

  private static JPanel buildSouthPanel() {
    return new FlowLayoutPanel(new JButton("Ok")).build();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
