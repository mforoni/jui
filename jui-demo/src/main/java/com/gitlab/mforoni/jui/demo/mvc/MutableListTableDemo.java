package com.gitlab.mforoni.jui.demo.mvc;

import com.github.mforoni.jsupport.MutablePerson;
import com.github.mforoni.jsupport.MutablePersons;
import com.github.mforoni.jsupport.Person.Gender;
import com.gitlab.mforoni.jui.Alignment;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.mvc.ListTableModel;
import com.gitlab.mforoni.jui.mvc.TableController;
import com.gitlab.mforoni.jui.mvc.TableHeader;
import com.gitlab.mforoni.jui.mvc.TableOptions;
import com.gitlab.mforoni.jui.mvc.TableView;
import java.util.Date;
import java.util.List;
import javax.swing.SwingUtilities;
import org.joda.time.LocalDate;

final class MutableListTableDemo {
  private MutableListTableDemo() {}

  private static void createAndShowGUI() {
    final TableView view =
        new TableView(new TableOptions.Builder(Alignment.CENTER).autosize().build());
    final ListTableModel<MutablePerson> tableModel = new ListTableModel<>(new PersonTableHeader());
    final TableController<MutablePerson> controller = new TableController<>(view, tableModel);
    controller.addInsertRowAction(e -> {
      controller.getTableModel().add(new MutablePerson());
    });
    final List<MutablePerson> persons = MutablePersons.ARRAY_LIST;
    controller.setList(persons);
    final Frame frame =
        new Frame.Builder("Mutable Table Demo").centerPanel(view.getPanel()).build();
    frame.show();
  }

  private static final class PersonTableHeader implements TableHeader<MutablePerson> {
    private static final String[] HEADER =
        {"LAST_NAME", "FIRST_NAME", "GENDER", "DATE_BIRTH", "EMAIL"};
    private static final Class<?>[] CLASSES =
        {String.class, String.class, Gender.class, LocalDate.class, String.class};

    @Override
    public int getColumnCount() {
      return HEADER.length;
    }

    @Override
    public String getColumnName(final int columnIndex) {
      return HEADER[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(final int columnIndex) {
      return CLASSES[columnIndex];
    }

    @Override
    public Object getValue(final MutablePerson person, final int columnIndex) {
      switch (columnIndex) {
        case 0:
          return person.getFirstName();
        case 1:
          return person.getLastName();
        case 2:
          return person.getGender();
        case 3:
          return person.getDateBirth();
        case 4:
          return person.getEmail();
        default:
          throw new IllegalStateException("Case not handled for columnIndex=" + columnIndex);
      }
    }

    @Override
    public void setValue(final MutablePerson person, final int columnIndex, final Object value) {
      switch (columnIndex) {
        case 0:
          person.setFirstName((String) value);
          break;
        case 1:
          person.setLastName((String) value);
          break;
        case 2:
          person.setGender((Gender) value);
          break;
        case 3:
          final Date date = (Date) value;
          person.setDateBirth(new LocalDate(date));
          break;
        case 4:
          person.setEmail((String) value);
          break;
        default:
          throw new IllegalStateException();
      }
    }

    @Override
    public boolean isEditable() {
      return true;
    }

    @Override
    public boolean isEditable(final int columnIndex) {
      return true;
    }
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
