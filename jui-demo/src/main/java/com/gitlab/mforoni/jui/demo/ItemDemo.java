package com.gitlab.mforoni.jui.demo;

import com.gitlab.mforoni.jui.Item;
import com.gitlab.mforoni.jui.model.SortedComboBoxModel;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * See this
 * <a href="https://tips4java.wordpress.com/2013/02/18/combo-box-with-hidden-data/">link</a>.
 *
 * @author Rob Camick
 */
final class ItemDemo extends JPanel {
  /** */
  private static final long serialVersionUID = 7494912400672534055L;

  public ItemDemo() {
    setBorder(new EmptyBorder(10, 10, 10, 10));
    setLayout(new GridLayout(0, 2, 10, 5));
    add(new JLabel("Item Basic Use"));
    add(new JLabel("Item As Wrapper"));
    // Create combo box showing basic use of the Item class
    final JComboBox<Item<String>> comboBox = new JComboBox<>();
    add(comboBox);
    comboBox.addItem(new Item<>("CA", "Canada"));
    comboBox.addItem(new Item<>("GB", "United Kingdom"));
    comboBox.addItem(new Item<>("US", "United States"));
    comboBox.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final JComboBox<Item<String>> comboBox = (JComboBox<Item<String>>) e.getSource();
        final Item<String> item = (Item<String>) comboBox.getSelectedItem();
        final String code = item.getValue();
        System.out.println(code);
      }
    });
    // Create combo box using Item class as a wrapper
    final JComboBox<Item<Car>> comboBox2 = new JComboBox<>();
    add(comboBox2);
    final Car c1 = new Car("Ford", "Crown Victoria");
    final Car c2 = new Car("Ford", "Focus");
    final Car c3 = new Car("Ford", "Fusion");
    final Car c4 = new Car("Honda", "Civic");
    final Car c5 = new Car("Honda", "Fit");
    final Car c6 = new Car("Nissan", "Altima");
    final Car c7 = new Car("Toyota", "Camry");
    final Car c8 = new Car("Toyota", "Corolla");
    comboBox2.setModel(new SortedComboBoxModel<Item<Car>>());
    comboBox2.addItem(new Item<>(c1, c1.getModel()));
    comboBox2.addItem(new Item<>(c2, c2.getModel()));
    comboBox2.addItem(new Item<>(c3, c3.getModel()));
    comboBox2.addItem(new Item<>(c4, c4.getModel()));
    comboBox2.addItem(new Item<>(c5, c5.getModel()));
    comboBox2.addItem(new Item<>(c6, c6.getModel()));
    comboBox2.addItem(new Item<>(c7, c7.getModel()));
    comboBox2.addItem(new Item<>(c8, c8.getModel()));
    comboBox2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final JComboBox<Item<Car>> comboBox = (JComboBox<Item<Car>>) e.getSource();
        final Item<Car> item = (Item<Car>) comboBox.getSelectedItem();
        final Car car = item.getValue();
        System.out.println(car);
      }
    });
  }

  private static void createAndShowUI() {
    JFrame.setDefaultLookAndFeelDecorated(true);
    final JFrame frame = new JFrame("ItemTest");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.add(new ItemDemo());
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
  }

  static class Car {
    private final String make;
    private final String model;

    public Car(final String make, final String model) {
      this.make = make;
      this.model = model;
    }

    public String getMake() {
      return make;
    }

    public String getModel() {
      return model;
    }

    @Override
    public String toString() {
      return make + " : " + model;
    }
  }

  public static void main(final String[] args) {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowUI();
      }
    });
  }
}
