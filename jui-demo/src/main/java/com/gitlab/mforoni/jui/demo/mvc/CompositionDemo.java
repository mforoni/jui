package com.gitlab.mforoni.jui.demo.mvc;

import com.github.mforoni.jsupport.Author;
import com.github.mforoni.jsupport.Authors;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.mvc.FilterableTableController;
import com.gitlab.mforoni.jui.mvc.FilterableTableView;
import com.gitlab.mforoni.jui.mvc.form.FormController;
import com.gitlab.mforoni.jui.mvc.form.FormElement.ComponentType;
import com.gitlab.mforoni.jui.mvc.form.FormModel;
import java.util.List;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

final class CompositionDemo {
  private CompositionDemo() {}

  private static void createAndShowGUI() {
    // Table
    final FilterableTableView tableView = new FilterableTableView();
    final List<Author> authors = Authors.ARRAY_LIST;
    final DefaultTableModel tableModel = Persons.defaultTableModel(authors);
    final FilterableTableController tableController =
        new FilterableTableController(tableView, tableModel);
    // Form
    final FormModel formModel = new FormModel.Builder(). //
        add("Books:", ComponentType.JLIST, null, null). //
        add("Date Birth:", ComponentType.JLABEL, null, null).build();
    final FormController<FormModel> formController = new FormController<>(formModel);
    tableController.getView().getTable().getSelectionModel()
        .addListSelectionListener(new ListSelectionListener() {
          @Override
          public void valueChanged(final ListSelectionEvent e) {
            final Author author =
                authors.get(tableController.getView().getTable().getSelectedRow());
            formController.getModel().getElements().get(0).setValue(author.getBooks());
            formController.getModel().getElements().get(1)
                .setValue(author.getDateBirth().toString());
          }
        });
    // Frame
    final Frame frame = new Frame.Builder("Composition Demo").centerPanel(tableView.getPanel())
        .southPanel(formController.getView().getJPanel()).build();
    tableController.getView().getTable().setRowSelectionInterval(0, 0);
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
