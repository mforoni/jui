package com.gitlab.mforoni.jui.demo.layout;

import com.gitlab.mforoni.jui.Colors;
import com.gitlab.mforoni.jui.Fonts;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.layout.FlowLayoutPanel;
import com.gitlab.mforoni.jui.layout.SpringLayoutPanel;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

final class SpringLayoutPanelDemo {
  private SpringLayoutPanelDemo() {} // Preventing instantiation

  private static void createAndShowGUI() {
    final JPanel southPanel =
        new FlowLayoutPanel().add(new JButton("Ok"), new JButton("Cancel")).build();
    final Frame frame = new Frame.Builder("SpringLayout Demo").centerPanel(centerPanel())
        .southPanel(southPanel).build();
    frame.get().setBackground(Colors.AZURE); // Not working: wrong way to do that
    frame.get().setFont(Fonts.TAHOMA_16);
    frame.show();
  }

  private static JPanel centerPanel() {
    final Border border = BorderFactory.createLineBorder(Color.BLUE, 1);
    final JLabel label1 = new JLabel("Some text");
    label1.setBorder(border);
    final JLabel label2 = new JLabel("Some text");
    label2.setBorder(border);
    return new SpringLayoutPanel(2, 2). //
        add(new JLabel("Very long text let see if it can be handled well:")).add(label1). //
        add(new JLabel("Short text:")).add(label2).build();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
