package com.gitlab.mforoni.jui.demo.mvc;

import com.github.mforoni.jbasic.JLists;
import com.github.mforoni.jsupport.ImmutablePerson;
import com.github.mforoni.jsupport.ImmutablePersons;
import com.github.mforoni.jsupport.Person;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.OnClose;
import com.gitlab.mforoni.jui.model.ComboBoxModels;
import com.gitlab.mforoni.jui.mvc.SelectionController;
import com.gitlab.mforoni.jui.mvc.SelectionView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.SwingUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class SelectionFrameDemo {
  private static final Logger LOGGER = LoggerFactory.getLogger(SelectionFrameDemo.class);

  private SelectionFrameDemo() {}

  private static void createAndShowGUI() {
    final List<ImmutablePerson> persons = ImmutablePersons.ARRAY_LIST;
    final List<String> names = JLists.newSortedArrayList(persons, Person.GET_LASTNAME);
    final ComboBoxModel<String> model =
        ComboBoxModels.fromCollection(names, new String[names.size()]);
    final SelectionView<String> view = new SelectionView<>("Select a person:");
    final ActionListener action = new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final String item = view.getSelectedItem();
        LOGGER.info("Confirm button pressed: selected item {}", item);
      }
    };
    final SelectionController<String> controller = new SelectionController<>(view, model, action);
    final Frame frame = view.newFrame("Selection Form Demo", OnClose.DISPOSE);
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
