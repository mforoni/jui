package com.gitlab.mforoni.jui.demo.mvc.forms;

import com.github.mforoni.jbasic.reflect.JFields;
import com.github.mforoni.jsupport.MutablePerson;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.event.WindowListeners;
import com.gitlab.mforoni.jui.mvc.form.FormController;
import com.gitlab.mforoni.jui.mvc.form.InstanceFormModel;
import javax.swing.SwingUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class MutablePersonFormDemo {
  private static final Logger LOGGER = LoggerFactory.getLogger(MutablePersonFormDemo.class);

  private MutablePersonFormDemo() {}

  private static void createAndShowGUI() {
    final InstanceFormModel<MutablePerson> model =
        InstanceFormModel.fromType(MutablePerson.class, JFields.IS_NOT_STATIC);
    final FormController<InstanceFormModel<MutablePerson>> controller = new FormController<>(model);
    final Frame frame = new Frame.Builder("Mutable Person Form Demo")
        .centerPanel(controller.getView().getJPanel()).build();
    frame.get().addWindowListener(WindowListeners.disposeFrame(frame.get()));
    controller.addButtonActionListener(e -> {
      LOGGER.info("Confirm button pressed: created new MutablePerson {}",
          model.syncAndGetInstance());
    });
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
