package com.gitlab.mforoni.jui.demo.mvc;

import com.github.mforoni.jsupport.ImmutablePerson;
import com.github.mforoni.jsupport.ImmutablePersons;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.mvc.FilterableTableController;
import com.gitlab.mforoni.jui.mvc.FilterableTableView;
import java.util.List;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

final class FilterableTableDemo {
  private FilterableTableDemo() {}

  private static void createAndShowGUI() {
    final List<ImmutablePerson> persons = ImmutablePersons.ARRAY_LIST;
    final DefaultTableModel tableModel = Persons.defaultTableModel(persons);
    final FilterableTableView view = new FilterableTableView();
    final FilterableTableController controller = new FilterableTableController(view, tableModel);
    final Frame frame = new Frame.Builder("Filterable Table").centerPanel(view.getPanel()).build();
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
