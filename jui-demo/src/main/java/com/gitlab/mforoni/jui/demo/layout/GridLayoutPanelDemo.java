package com.gitlab.mforoni.jui.demo.layout;

import com.gitlab.mforoni.jui.Borders;
import com.gitlab.mforoni.jui.Fonts;
import com.gitlab.mforoni.jui.Frame;
import com.gitlab.mforoni.jui.JLabels;
import com.gitlab.mforoni.jui.layout.FlowLayoutPanel;
import com.gitlab.mforoni.jui.layout.GridLayoutPanel;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

final class GridLayoutPanelDemo {
  private GridLayoutPanelDemo() {} // Preventing instantiation

  private static void createAndShowGUI() {
    final JPanel northPanel = new FlowLayoutPanel(new JLabel("Testing Grid Layout")).build();
    final JPanel southPanel =
        new FlowLayoutPanel().add(new JButton("Ok"), new JButton("Cancel")).build();
    final JPanel centerPanel = new GridLayoutPanel(2, 2).border(Borders.EMPTY_BORDER)
        .add(new JLabel("Very long text let see if it can be handled well:"))
        .add(JLabels.bordered("Short text", Borders.RAISED_BEVEL_BORDER))
        .add(new JLabel("Short text:"))
        .add(JLabels.bordered("Some text", Borders.LOWERED_BEVEL_BORDER)).build();
    final Frame frame = new Frame.Builder("Grid Layout Demo").northPanel(northPanel)
        .southPanel(southPanel).centerPanel(centerPanel).build();
    frame.get().setBackground(Color.BLACK); // Not working
    frame.get().setFont(Fonts.TAHOMA_16);
    frame.show();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        createAndShowGUI();
      }
    });
  }
}
